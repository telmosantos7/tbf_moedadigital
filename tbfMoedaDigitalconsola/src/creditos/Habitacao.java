/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creditos;

import tbfmoedadigitalconsola.Cliente;

/**
 *
 * @author lucas
 */
public class Habitacao extends Credito {

    private final int codigoCredito = 02;
    private double valorCredito;
    private double duracao;
    private double valorMensal;
    private double taxas;
    private double valorTotal;

    Habitacao() {

    }

    public Habitacao(Cliente cliente) {
        super(cliente);
    }

    public Habitacao(Cliente cliente, int numCredito, double valorCredito) {
        super(cliente, numCredito);
        this.valorCredito = valorCredito;
    }

    public double getValorCredito() {
        return taxas;
    }

    public void aplicarTaxas() {
        double tan = 0.12;
        double taeg = 0.11;
        taxas = valorCredito * tan + valorCredito * taeg;
        valorTotal = valorCredito + taxas;
    }

    public double getValorMensal() {
        return valorMensal;
    }

    public void gerarValorMensal() {
        double valorMensalCalc = valorTotal / duracao + taxas;
        this.valorMensal = valorMensalCalc;
    }

    public void gerarDuracao() {
        double duracaoCalc = valorTotal / valorMensal;
        this.duracao = duracaoCalc;
    }

    public double getDuracao() {
        return duracao;
    }

    @Override
    public String toString() {
        return "Código de Crédtio: "
                + codigoCredito
                + ", "
                + "Número de crédito: "
                + numCredito
                + ", "
                + "Duração: "
                + getDuracao()
                + ", "
                + "valor mensal a ser pago: "
                + getValorMensal()
                + ", "
                + "Valor do crédito pedido"
                + getValorCredito()
                + ", "
                + "Valor total com as taxas aplicadas: "
                + valorTotal;
    }
}
