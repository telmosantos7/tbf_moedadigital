/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creditos;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import tbfmoedadigitalconsola.Cliente;

/**
 *
 * @author lucas
 */
public  class gestaoCreditos {

    private static final Scanner sc = new Scanner(System.in);
    private static final List<Credito> listaCreditos = new ArrayList<>();
    private static final Random random = new Random();
    private static Pessoal listaPessoal[];
    private static Habitacao listaHabitacao[];
    private static Automovel listaAutomovel[];
    private static char op;
    
    public static void gestaoCreditosArgs() {
        
    
    System.out.println("\t                      TBF Credit Managemment");
    System.out.println("\t=======================================================================");
    System.out.println("\t|                Bem vindo ao seu gestor de Créditos                  |");
    System.out.println("\t=======================================================================");
    System.out.println("\t| Prima (1) para Simular Crédito                                      |");
    System.out.println("\t| Prima (2) para Pedir Crédito                                        |");
    System.out.println("\t| Prima (3) para Visualizar Crédito/Base de dados                     |");
    System.out.println("\t| Prima (0) para Voltar ao Menu                                       |");
    System.out.println("\t=======================================================================");
    op = sc.next().charAt(0);

        switch (op) {
            case '1':

                simularCredito();
                break;

            case '2':
                pedirCredito();
                break;

            case '3':
                visualizarCreditos();
                break;

            case '0':

                System.out.println("A Regressar ao Menu");
                System.out.println("        . . .      ");
                break;
        }
    }
    /**
     *
     */
    public static void simularCredito() {
        System.out.println("Indique o tipo de crédito que deseja:");
        System.out.println("1-Pessoal");
        System.out.println("2-Automovel");
        System.out.println("3-Habitação");
        System.out.println("4-Voltar");
        char op = sc.next().charAt(0);

        switch (op) {
            case '1':
                System.out.println("Insira dados:");
                System.out.println("");
                System.out.print("Nome Completo: ");
                String nome = sc.next();
                System.out.println("Data de nascimento");
                String dataNasc = sc.next();
                System.out.print("BI ");
                String bi = sc.next();
                System.out.print("NIF: ");
                int nif = sc.nextInt();
                System.out.println("Indique o codigo de cliente");
                String codCliente = sc.next();
                System.out.println("Indique os rendimentos liquidos mensais: ");
                double rendimentosMensais = sc.nextDouble();
                System.out.println("Valor desejado: ");
                double valorDesejado = sc.nextDouble();
                System.out.println("Os dados foram gravados com sucesso, sua proposta vai ser analisada e entraremos em contacto");
                break;

            case '2':
                System.out.println("Insira dados:");
                System.out.println("");
                System.out.print("Nome Completo: ");
                nome = sc.next();
                System.out.print("BI ");
                bi = sc.next();
                System.out.print("NIF: ");
                nif = sc.nextInt();
                System.out.println("Indique o codigo de cliente");
                codCliente = sc.next();
                System.out.println("Indique os rendimentos liquidos mensais: ");
                rendimentosMensais = sc.nextDouble();
                System.out.println("Valor desejado: ");
                valorDesejado = sc.nextDouble();
                System.out.println("Os dados foram gravados com sucesso, sua proposta vai ser analisada e entraremos em contacto");
                break;

            case '3':
                System.out.println("Insira dados:");
                System.out.println("");
                System.out.print("Nome Completo: ");
                nome = sc.next();
                System.out.print("BI ");
                bi = sc.next();
                System.out.print("NIF: ");
                nif = sc.nextInt();
                System.out.println("Indique o codigo de cliente");
                codCliente = sc.next();
                System.out.println("Indique os rendimentos liquidos mensais: ");
                rendimentosMensais = sc.nextDouble();
                System.out.println("Valor desejado: ");
                valorDesejado = sc.nextDouble();
                System.out.println("Os dados foram gravados com sucesso, sua proposta vai ser analisada e entraremos em contacto");

                break;
            default:
                System.out.println("Opção inválida");

        }
    }

    /**
     * classe para pedir credito, cria um objeto com o tipo de credito escolhido
     */
    public static void pedirCredito() {
        System.out.println("Indique o tipo de crédito que deseja:");
        System.out.println("1-Pessoal");
        System.out.println("2-Automovel");
        System.out.println("3-Habitação");
        System.out.println("4-Voltar");
        char op = sc.next().charAt(0);

        switch (op) {
            case '1':
                System.out.println("Insira dados:");
                System.out.println("");
                System.out.print("Nome Completo: ");
                String nome = sc.next();
                sc.nextLine();
                System.out.print("BI ");
                String bi = sc.next();
                sc.nextLine();
                System.out.println("Indique a data de nascimento:");
                String dataNasc = sc.next();
                System.out.print("NIF: ");
                int nif = sc.nextInt();
                sc.nextLine();
                System.out.println("Indique o código de cliente");
                String codCliente = sc.next();
                sc.nextLine();
                System.out.println("Indique os rendimentos liquidos mensais: ");
                double rendimentosMensais = sc.nextDouble();
                sc.nextLine();
                System.out.println("Valor desejado: ");
                double valorDesejado = sc.nextDouble();

                if (rendimentosMensais > 900) {

                    Cliente cliente = new Cliente();
                    cliente.setCodCliente(codCliente);
                    cliente.setNome(nome);
                    cliente.setDataNascm(dataNasc);
                    cliente.setNif(nif);

                    int numCredito = random.nextInt(1000);

                    Credito credito = new Credito(cliente, numCredito);

                    Pessoal pessoal = new Pessoal(cliente, numCredito, valorDesejado);

                    pessoal.aplicarTaxas();
                    pessoal.gerarValorMensal();
                    pessoal.gerarDuracao();

                    listaCreditos.add(pessoal);

                    System.out.println(pessoal);
                } else {
                    System.out.println("Infelizmente seu pedido não foi aprovado");
                }
                break;

            case '2':
                System.out.println("Insira dados:");
                System.out.println("");
                System.out.print("Nome Completo: ");
                nome = sc.next();
                sc.nextLine();
                System.out.print("BI ");
                bi = sc.next();
                sc.nextLine();
                System.out.println("Indique a data de nascimento:");
                dataNasc = sc.next();
                sc.nextLine();
                System.out.print("NIF: ");
                nif = sc.nextInt();
                sc.nextLine();
                System.out.println("Indique o código de cliente");
                codCliente = sc.next();
                sc.nextLine();
                System.out.println("Indique os rendimentos liquidos mensais: ");
                rendimentosMensais = sc.nextDouble();
                sc.nextLine();
                System.out.println("Valor desejado: ");
                valorDesejado = sc.nextDouble();
                System.out.println("Os dados foram gravados com sucesso, sua proposta vai ser analisada e entraremos em contacto");

                if (rendimentosMensais > 900) {

                    Cliente cliente = new Cliente();
                    cliente.setCodCliente(codCliente);
                    cliente.setNome(nome);
                    cliente.setDataNascm(dataNasc);
                    cliente.setNif(nif);

                    int numCredito = random.nextInt(1000);

                    Credito credito = new Credito(cliente, numCredito);

                    Automovel automovel = new Automovel(cliente, numCredito, valorDesejado);

                    automovel.aplicarTaxas();
                    automovel.gerarValorMensal();
                    automovel.gerarDuracao();

                    listaCreditos.add(automovel);

                    System.out.println(automovel);

                    listaCreditos.add(automovel);

                    System.out.println(automovel);
                } else {
                    System.out.println("Infelizmente seu pedido não foi aprovado");
                }

                break;

            case '3':
                System.out.println("Insira dados:");
                System.out.println("");
                System.out.print("Nome Completo: ");
                nome = sc.next();
                System.out.print("BI ");
                bi = sc.next();
                sc.nextLine();
                System.out.println("Indique a data de nascimento:");
                dataNasc = sc.next();
                sc.nextLine();
                System.out.print("NIF: ");
                nif = sc.nextInt();
                sc.nextLine();
                System.out.println("Indique o código de cliente");
                codCliente = sc.next();
                sc.nextLine();
                System.out.println("Indique os rendimentos liquidos mensais: ");
                rendimentosMensais = sc.nextDouble();
                sc.nextLine();
                System.out.println("Valor desejado: ");
                valorDesejado = sc.nextDouble();
                System.out.println("Os dados foram gravados com sucesso, sua proposta vai ser analisada e entraremos em contacto");

                if (rendimentosMensais > 900) {

                    Cliente cliente = new Cliente();
                    cliente.setCodCliente(codCliente);
                    cliente.setNome(nome);
                    cliente.setDataNascm(dataNasc);
                    cliente.setNif(nif);

                    int numCredito = random.nextInt(1000);

                    Credito credito = new Credito(cliente, numCredito);

                    Habitacao habitacao = new Habitacao(cliente, numCredito, valorDesejado);

                    habitacao.aplicarTaxas();
                    habitacao.gerarValorMensal();
                    habitacao.gerarDuracao();

                    listaCreditos.add(habitacao);

                    System.out.println(habitacao);

                    listaCreditos.add(habitacao);

                    System.out.println(habitacao);
                } else {
                    System.out.println("Infelizmente seu pedido não foi aprovado");
                }

                break;

            case '4':

                break;

            default:
                System.out.println("Opção inválida");

        }
    }

    /**
     * vizualizar os creditos e guardar em ficheiros/ ler de ficheiros
     */
    public static void visualizarCreditos() {
        char op, op1, op2;

        System.out.println("1-Guardar dados na base de dados");
        System.out.println("2-Visualizar créditos na base de dados");
        System.out.println("3-Visualizar créditos(em memória)");
        op = sc.next().charAt(0);

        switch (op) {
            case '1':
                System.out.println("1-Guardar créditos Pessoais");
                System.out.println("2-Guardar créditos Automovel");
                System.out.println("3-Guardar créditos Habitação");
                op1 = sc.next().charAt(0);

                switch (op1) {
                    case '1':
                        //Creditos Pessoais
                        int numCreditosPessoais;
                        numCreditosPessoais = 0;
                        System.out.println("\n-->Iniciando a gravação de dados!\n");
                        try (FileWriter fw = new FileWriter("CreditosPessoal.txt", false); // cria o ficehrio "CreditosPessoal".txt 
                                BufferedWriter bw = new BufferedWriter(fw);
                                PrintWriter pw = new PrintWriter(bw)) {
                            for (int i = 0; i < numCreditosPessoais; i++) {   // percorre o array         
                                Pessoal p = listaPessoal[i];         // variavel de tipo Cliente 
                                String linha = (p.getNumCredito() + ";" + p.getDuracao() + ";" + p.getValorCredito() + ";" + p.getValorMensal() + ";" + p.getTaxas() + ";" + p.getCliente()); // constroi a linha que vai para o ficheiro
                                System.out.println("Credito{ " + p.getNumCredito() + " | " + p.getDuracao() + " | valor pedido: " + p.getValorCredito()
                                        + " | valor a pagar mensalmente " + p.getValorMensal() + " | taxas: " + p.getTaxas() + " | cliente: " + p.getCliente() + " }"); // mostra a linha construida
                                pw.println(linha);         // escreve a linha no ficheiro

                            }
                            pw.close();  // fecha o PrintWriter
                            bw.close();   // fecha o BufferedWriter
                            System.out.println("Gravado com sucesso!\n");

                        } catch (IOException e) {   // mostra os erros
                            System.out.println("ERRO: " + e.getMessage());
                        }

                        break;

                    case '2':
                        //Creditos Automoveis
                        int numCreditosAutomoveis;
                        numCreditosAutomoveis = 0;
                        System.out.println("\n-->Iniciando a gravação de dados!\n");
                        try (FileWriter fw = new FileWriter("CreditoAutomovel.txt", false); // cria o ficehrio "CreditosAutomovel".txt 
                                BufferedWriter bw = new BufferedWriter(fw);
                                PrintWriter pw = new PrintWriter(bw)) {
                            for (int i = 0; i < numCreditosAutomoveis; i++) {   // percorre o array         
                                Automovel a = listaAutomovel[i];         // variavel de tipo Cliente 
                                String linha = (a.getNumCredito() + ";" + a.getDuracao() + ";" + a.getValorCredito() + ";" + a.getValorMensal() + ";" + a.getTaxas() + ";" + a.getCliente()); // constroi a linha que vai para o ficheiro
                                System.out.println("Credito{ " + a.getNumCredito() + " | " + a.getDuracao() + " | valor pedido: " + a.getValorCredito()
                                        + " | valor a pagar mensalmente " + a.getValorMensal() + " | taxas: " + a.getTaxas() + " | cliente: " + a.getCliente() + " }"); // mostra a linha construida
                                pw.println(linha);         // escreve a linha no ficheiro

                            }
                            pw.close();  // fecha o PrintWriter
                            bw.close();   // fecha o BufferedWriter
                            System.out.println("Gravado com sucesso!\n");

                        } catch (IOException e) {   // mostra os erros
                            System.out.println("ERRO: " + e.getMessage());
                        }

                        break;

                    case '3':
                        //Creditos Habitação

                        int numCreditosHabitacao;
                        numCreditosHabitacao = 0;
                        System.out.println("\n-->Iniciando a gravação de dados!\n");
                        try (FileWriter fw = new FileWriter("CreditosPessoais.txt", false); // cria o ficehrio "CreditosAutomovel".txt 
                                BufferedWriter bw = new BufferedWriter(fw);
                                PrintWriter pw = new PrintWriter(bw)) {
                            for (int i = 0; i < numCreditosHabitacao; i++) {   // percorre o array         
                                Habitacao h = listaHabitacao[i];         // variavel de tipo Cliente 
                                String linha = (h.getNumCredito() + ";" + h.getDuracao() + ";" + h.getValorCredito() + ";" + h.getValorMensal() + ";" + ";" + h.getCliente()); // constroi a linha que vai para o ficheiro
                                System.out.println("Credito{ " + h.getNumCredito() + " | " + h.getDuracao() + " | valor pedido: " + h.getValorCredito()
                                        + " | valor a pagar mensalmente " + h.getValorMensal() + " | taxas: " + " | cliente: " + h.getCliente() + " }"); // mostra a linha construida
                                pw.println(linha);         // escreve a linha no ficheiro

                            }
                            pw.close();  // fecha o PrintWriter
                            bw.close();   // fecha o BufferedWriter
                            System.out.println("Gravado com sucesso!\n");

                        } catch (IOException e) {   // mostra os erros
                            System.out.println("ERRO: " + e.getMessage());
                        }

                        break;
                }

                break;

            case '2':

                System.out.println("Not working yet, sorry");

                break;

            case '3':
                for (Credito x : listaPessoal) {
                    System.out.println(x);
                }
                for (Credito x : listaAutomovel) {
                    System.out.println(x);
                }
                for (Credito x : listaHabitacao) {
                    System.out.println(x);
                }
                break;

            default:
                System.out.println("Opção inválida");

                break;

        }

    }
}
