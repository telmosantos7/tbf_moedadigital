/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creditos;

import java.util.Random;
import tbfmoedadigitalconsola.Cliente;

/**
 *
 * @author lucas
 */
public class Credito {

    int numCredito;
    private Cliente cliente;
    Random random = new Random();
   
    Credito() {

    }

    public Credito(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public Credito (Cliente cliente, int numCredito){
        this.cliente = cliente;
        this.numCredito = numCredito;
    }

    public int getNumCredito() {
        return numCredito;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void gerarNumCredito(int numCredito) {
        numCredito = random.nextInt(numCredito);
        this.numCredito = numCredito;
    }
    
    public String toString(){
    return "Cliente: "
           + cliente 
           +", "
           +"Número do crédito: "
           + numCredito;
    }
}
