/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creditos;

import tbfmoedadigitalconsola.Cliente;

/**
 *
 * @author lucas
 */
public class Pessoal extends Credito {

    private final int codigoCredito = 01;
    private double valorCredito;
    private double duracao;
    private double valorMensal;
    private double taxas;
    private double valorTotal;

    public Pessoal() {

    }

    public Pessoal(Cliente cliente, int numCredito, double valorCreditoDesejado) {
        super(cliente, numCredito);
        this.valorCredito = valorCreditoDesejado;
    }

    public Pessoal(Cliente cliente) {
        super(cliente);

    }

    public double getValorCredito() {
        return valorCredito;
    }
    
    public void aplicarTaxas() {
        double tan = 0.12;
        double taeg = 0.11;
        taxas = valorCredito * tan + valorCredito * taeg;
        valorTotal = valorCredito + taxas;
    }

    public double getTaxas() {
        return taxas;
    }

    public void gerarValorMensal() {
        double valorMensalCalc = (valorTotal / duracao ) + taxas;
        this.valorMensal = valorMensalCalc;
    }

       public double getValorMensal() {
        return valorMensal;
    }
       
       
    public void gerarDuracao() {
        double duracaoCalc = valorTotal / valorMensal;
        this.duracao = duracaoCalc;
    }

    public double getDuracao() {
        return duracao;
    }

    @Override
    public String toString() {
        return "Código de Crédtio: "
                + codigoCredito
                + ", "
                +"Número de crédito: "
                + numCredito
                +", "
                + "Duração: "
                + getDuracao()
                + ", "
                + "valor mensal a ser pago: "
                + getValorMensal()
                + ", "
                + "Valor do crédito pedido"
                + getValorCredito()
                + ", "
                + "Valor total com as taxas aplicadas: "
                + valorTotal;
    }
}
