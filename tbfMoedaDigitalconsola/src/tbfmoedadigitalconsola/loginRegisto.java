package tbfmoedadigitalconsola;

/**
 *
 * @author bruno
 */
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class loginRegisto {
    
    private static final Scanner scan = new Scanner(System.in);
    private static String[] logins = new String[100];
    private static String[] palavrasChave = new String[100];
    private static int tamanhototal;
    protected static boolean acesso =false;

    static void Registo() {
        boolean verifica = true;
        String nome = null, chave = null;
        int existe = 0;
            
        do{ 
            System.out.println("Insira um User válido: ");
            nome = scan.next();
            for (int i = 0; i <= tamanhototal; i++) {
                verifica = nome.equals(logins[i]);
                
                if (verifica == true) {
                    System.out.println("\t ERRO o User que escolheu já existente!");
                    existe++;
                    break;
                }else if(verifica == false){
                    verifica = false;
                }
            }
        } while(verifica == true);
            
            System.out.println("Insira uma Password válida: ");
            chave = scan.next();

            if (verifica == false) {
                logins[tamanhototal] = nome;
                palavrasChave[tamanhototal] = chave;
//                System.out.println("nome: " + logins[tamanhototal] + " e password " + palavrasChave[tamanhototal] + " adicionado com sucesso");
                guardarDados();
            }      
    }

    static void carregarDados() {
        tamanhototal = 0;
        int add = 0, add1 = 0;
        String[] dadosTemp = new String[100];
        String path1 = Paths.get("").toAbsolutePath().toString();
        String path = path1 + "\\DadosRegistos.txt";
//        System.out.println("\nA Carregar dados");
//        System.out.println("    . . .         ");
        try {
            List<String> allLines = Files.readAllLines(Paths.get(path));
            for (String line : allLines) {
                dadosTemp[add] = line;
                String[] arrOfStr = dadosTemp[add].split(";", 2);
                logins[add] = arrOfStr[0];
                palavrasChave[add] = arrOfStr[1];
                add++;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Leitura dos dados com ERROS!");
        }
        for (int i = 0; i < dadosTemp.length; i++) {
            if (dadosTemp[i] != null && dadosTemp[i] != "") {
                tamanhototal++;
            }
        }
    }

    private static void guardarDados() {

        String path1 = Paths.get("").toAbsolutePath().toString();
        String path = path1 + "\\DadosRegistos.txt";
        try (FileWriter fw = new FileWriter(path, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            for (int i = 0; i <= tamanhototal; i++) {
                String linha = (logins[i] + ";" + palavrasChave[i]);
                out.println(linha);         // escreve a linha no ficheiro

            }
            out.close();  // fecha o PrintWriter
            bw.close();   // fecha o BufferedWriter
            System.out.println("Gravado com sucesso!\n");

        } catch (IOException e) {
            System.out.println(e.getMessage()); // imprime os erros
            System.out.println("Devido a erros nao foi possivel guardar!");
        }
    }

    static boolean Login() {
        boolean verifica = false;

        String nome, chave;
        do {
            int existe = 0;
            System.out.println("User: ");
            nome = scan.next();
            System.out.println("Password: ");
            chave = scan.next();
            for (int i = 0; i <= tamanhototal; i++) {
                verifica = nome.equals(logins[i]) && chave.equals(palavrasChave[i]);
                if (verifica) {
                    System.out.println("\nLogin correcto!\nBem Vindo!");
                    System.out.println("\n");
                    existe++;
                }
            acesso = true;    
            }
            if (verifica == false && existe == 0) {
                acesso = false;
            }
        } while (verifica == true);
        return acesso;
    }

}
