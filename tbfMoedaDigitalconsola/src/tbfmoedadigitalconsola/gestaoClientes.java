package tbfmoedadigitalconsola;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author bruno
 */
public class gestaoClientes {

    private static final Scanner scan = new Scanner(System.in);
    private static final Random rand = new Random();

    public static void TBFCM() {
//        Cliente[] listaClientes = new Cliente[50];
        int numClientes = 0;

        String nome = null, codCliente, dataNascm;
        int nif;
        boolean rr = true;

        while (rr == true) {
            // menu do Client
            System.out.println("\t                      TBF Client Managemment");
            System.out.println("\t=======================================================================");
            System.out.println("\t|                Bem vindo ao seu gestor de Clientes                  |");
            System.out.println("\t=======================================================================");
            System.out.println("\t| Prima (1) para Criar cliente                                        |");
            System.out.println("\t| Prima (2) para Alterar cliente                                      |");
            System.out.println("\t| Prima (3) para Remover cliente                                      |");
            System.out.println("\t| Prima (0) para Voltar ao Menu                                       |");
            System.out.println("\t=======================================================================");
            char escolha =  scan.next().charAt(0);  
            
            ArrayList<Cliente> cliente = new ArrayList<>();

            switch (escolha) {

                case '1':
                    for (int i = 0; i < 2; i++) {
                        System.out.println("Qual seu nome? ");
                        nome += scan.nextLine();
                        System.out.println("Qual seu nif? ");
                        nif = scan.nextInt();
                        System.out.println("Qual sua data nascimento? ");
                        dataNascm = scan.next();
                        codCliente = "TBF00" + rand.nextInt(100000);
                        cliente.add(new Cliente(nome, nif, dataNascm, codCliente));

                    }

                    try (BufferedWriter buffWrite = new BufferedWriter(new FileWriter("Clientes.txt", true))) {
                        for (Cliente c : cliente) {
                            buffWrite.write(c.getNome() + ";" + c.getNif() + ";" + c.getDataNascm() + ";" + c.getCodCliente());
                            buffWrite.newLine();
                        }
                        buffWrite.close();

                    } catch (IOException e) {
                        System.out.println("Erro: "+ e.getMessage());
                    }
                    break;

                case '2': // Alterar dados do Cliente
                    try (BufferedReader buffReader = new BufferedReader(new FileReader("Clientes.txt"))) {
                        cliente = new ArrayList();
                        String linha;
                        while ((linha = buffReader.readLine()) != null) {
                            String[] split = linha.split(";");
                            System.out.println(linha);
                            cliente.add(new Cliente(split[0], Integer.valueOf(split[1]), (split[2]), split[3]));

                        }
                        buffReader.close();
                    } catch (IOException e) {
                        System.out.println("Erro: "+ e.getMessage());
                    }

                    System.out.println("Qual codigo deseja alterar");
                    String codAlterar = scan.next();
                    for (Cliente c : cliente) {
                        if (c.getCodCliente().equals(codAlterar)) {

                            System.out.println("\t=======================================================================");
                            System.out.println("\t| Prima (1) para Alterar Nome                                         |");
                            System.out.println("\t| Prima (2) para Alterar Nif                                          |");
                            System.out.println("\t| Prima (3) para Alterar Data de Nascimento                           |");
                            System.out.println("\t| Prima (4) para Alterar todos os dados                               |");
                            System.out.println("\t| Prima (0) para voltar ao Menu anterior                              |");
                            System.out.println("\t=======================================================================");
                            char escolha1 = scan.next().charAt(0);
                            switch (escolha1) {
                                
                                case '1':
                                    System.out.println("Vai alterar o nome!".concat(" Para que nome deseja alterar?"));
                                    String NomeAlterar;
                                    NomeAlterar = scan.nextLine();
                                    NomeAlterar += scan.nextLine();
                                    c.setNome(NomeAlterar);
                                    System.out.println("Nome alterado!");
                                    break;
                                
                                case '2':
                                    System.out.println("Vai alterar o Nif: ".concat(" Para que nif deseja alterar?"));
                                    int nifAlterar = scan.nextInt();
                                    c.setNif(nifAlterar);
                                    System.out.println("Nif alterado!");
                                    break;
                                
                                case '3':
                                    System.out.println("Vai alterar a Data Nascimento: ".concat(" Para que data deseja alterar?"));
                                    String dataAlterar = scan.next();
                                    c.setDataNascm(dataAlterar);
                                    System.out.println("Data alterada!");
                                    break;
                                
                                case '4':
                                    System.out.println("Vai alterar todo o cliente");
                                    System.out.println("Vai alterar o nome!".concat(" Para que nome deseja alterar?"));
                                    String NomeT;
                                    NomeT = scan.nextLine();
                                    NomeT += scan.nextLine();
                                    c.setNome(NomeT);
                                    System.out.println("Vai alterar o Nif: ".concat(" Para que nif deseja alterar?"));
                                    int nifT = scan.nextInt();
                                    c.setNif(nifT);
                                    System.out.println("Vai alterar a Data Nascimento: ".concat(" Para que data deseja alterar?"));
                                    String dataT = scan.next();
                                    c.setDataNascm(dataT);
                                    System.out.println("Data alterada!");
                                    break;
                                
                                case '0' :     
                                    System.out.println("A voltar ao menu anterior!");
                                    break;
                                default:  System.out.println("Opção indisponível!"); 
                                    break; 
                            }

                        }
                    }

                try (BufferedWriter buffWrite = new BufferedWriter(new FileWriter("Clientes.txt", false))) {
                    for (Cliente c : cliente) {
                        buffWrite.write(c.getNome() + ";" + c.getNif() + ";" + c.getDataNascm() + ";" + c.getCodCliente());
                        buffWrite.newLine();
                    }
                    buffWrite.close();

                } catch (IOException e) {
                    System.out.println("Erro: "+ e.getMessage());
                }
                break;

                case '3': // remover Cliente
                    try (BufferedReader buffReader = new BufferedReader(new FileReader("Clientes.txt"))) {
                        cliente = new ArrayList();
                        String linha;
                        while ((linha = buffReader.readLine()) != null) {
                            String[] split = linha.split(";");
                            System.out.println(linha);
                            cliente.add(new Cliente(split[0], Integer.valueOf(split[1]), (split[2]), split[3]));

                        }
                        buffReader.close();
                    } catch (IOException e) {
                        System.out.println("Erro: "+ e.getMessage());
                    }
                    System.out.println("Qual codigo deseja alterar");
                    String codRemover = scan.next();
                    for (int i = 0; i < cliente.size(); i++) {
                        if (cliente.get(i) != null) {
                            if (cliente.get(i).getCodCliente().equals(codRemover)) {
                                cliente.remove(cliente.get(i));
                            }
                        }   
                    }
                    try (BufferedWriter buffWrite = new BufferedWriter(new FileWriter("Clientes.txt", false))) {
                    for (Cliente c : cliente) {
                        buffWrite.write(c.getNome() + ";" + c.getNif() + ";" + c.getDataNascm() + ";" + c.getCodCliente());
                        buffWrite.newLine();
                    }
                    buffWrite.close();

                } catch (IOException e) {
                    System.out.println("Erro: "+ e.getMessage());
                }
                case '0' :     
                    System.out.println("A voltar ao menu!");
                    rr = false;
                    break;
                default:  System.out.println("Opção indisponível!"); 
                    break;        
             }
        }
    }
}

