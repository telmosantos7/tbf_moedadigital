package tbfmoedadigitalconsola;

/**
 *
 * @author bruno
 */
public class Cliente {

    private String nome;
    private int nif;
    private String dataNascm;
    private String codCliente;

    Cliente(String nome, int nif, String dataNascm, String codCliente) {
        this.nome = nome;
        this.nif = nif;
        this.dataNascm = dataNascm;
        this.codCliente = codCliente;
    }

    public Cliente() {
    }

    Cliente(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    Cliente(String string, String string0, Integer valueOf) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Nome get
     *
     * @return nome do Cliente
     */
    public String getNome() {
        return nome;
    }

    /**
     * Nome set
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Nif get
     *
     * @return nif do Cliente
     */
    public int getNif() {
        return nif;
    }

    /**
     * Nif set
     *
     * @param nif
     */
    public void setNif(int nif) {
        this.nif = nif;
    }

    /**
     * Data de nascimento get
     *
     * @return data de nascimento do Cliente
     */
    public String getDataNascm() {
        return dataNascm;
    }

    /**
     * Data de nascimento set
     *
     * @param dataNascm
     */
    public void setDataNascm(String dataNascm) {
        this.dataNascm = dataNascm;
    }

    /**
     * Código do Cliente get
     *
     * @return código do Cliente
     */
    public String getCodCliente() {
        return codCliente;
    }

    /**
     * Código do Cliente set
     *
     * @param codCliente
     */
    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }


    /**
     * toString
     *
     * toString é usado para formatar os dados do Cliente Este método organiza
     * os dados do Cliente
     *
     * @return
     */
    @Override
    public String toString() {
        return "Cliente:" + "nome=" + nome + ";" + " nif=" + nif + ";" + "dataNascimento = " + dataNascm + ";" + "codCliente=" + codCliente + '.';
    }

    
}
