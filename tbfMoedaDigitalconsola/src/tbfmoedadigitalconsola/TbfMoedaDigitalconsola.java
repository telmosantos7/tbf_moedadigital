package tbfmoedadigitalconsola;

import creditos.gestaoCreditos;
import java.util.Scanner;
import static tbfmoedadigitalconsola.loginRegisto.acesso;

/**
 *
 * @author ASUS
 */
public class TbfMoedaDigitalconsola {

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {

        char opcao, op;

        while (true) {
            loginRegisto.carregarDados();
            System.out.println("\t                         TBF BANK SYSTEM");
            System.out.println("\t=======================================================================");
            System.out.println("\t|                    Bem vindo ao seu Banco TBF                       |");
            System.out.println("\t=======================================================================");
            System.out.println("\t| Prima (1) REGIST                                                    |");
            System.out.println("\t| Prima (2) LOGIN                                                     |");
            System.out.println("\t| Prima (0) Sair                                                      |");
            System.out.println("\t=======================================================================");
            opcao = scan.next().charAt(0);

            switch (opcao) {
                case '0':

                    System.out.println("Até á próxima!");
                    System.exit(0);

                case '1':

                    loginRegisto.Registo();
                    break;

                case '2':

                    loginRegisto.Login();
                        if(acesso == true){
                            System.out.println("\t=======================================================================");
                            System.out.println("\t| Prima (1) para Gestão Clientes                                      |");
                            System.out.println("\t| Prima (2) para Gestão Créditos                                      |");
                            System.out.println("\t| Prima (0) para voltar ao Menu anterior                              |");
                            System.out.println("\t=======================================================================");
                            char select =  scan.next().charAt(0); 
                            
                            switch(select){
                            
                                case '1': 
                                    gestaoClientes.TBFCM(); //gestão Clientes
                                    break;
                                
                                case '2': 
                                    gestaoCreditos.gestaoCreditosArgs(); // gestão Créditos
                                    break;
                                
                                case '0':     
                                    System.out.println("A voltar ao Menu anterior!");
                                    break;
                                
                                default:  System.out.println("Opção indisponível!"); 
                                    break; 
                            }
                        }
                        else{
                            System.out.println("User ou Password incorreta!");
                        }
                    break;

                default:
                    System.out.println("Opção indisponível!");
                    break;
            }
        }
    }
}
