/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.openjfx.tbf_moedadigital.UtilizadorDAO.getId;

/**
 *
 * @author lucas
 */
//Criar metodos -> Create, Update, Delete, Select
public class MovimentosDao {

    public static void insertMovimentoTrasnferencia(double valor, String Iban) {
        PreparedStatement st = null;
        PreparedStatement stt = null;
        PreparedStatement ts = null;
        Movimentos obj = new Movimentos(valor, Iban);
        int id_origem = Helper.getLoginUser().getId_utilizador();
        try {
            Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("UPDATE LAPR2_G5.Utilizador SET saldo_utilizador = saldo_utilizador + ? WHERE iban_utilizador=?");
            st.setDouble(1, valor);
            st.setString(2, Iban);
            st.executeUpdate();

            stt = conn.prepareStatement("UPDATE LAPR2_G5.Utilizador SET saldo_utilizador = saldo_utilizador - ? WHERE id_utilizador = ?");
            stt.setDouble(1, valor);
            stt.setInt(2, getId());
            stt.executeUpdate();

            ts = conn.prepareStatement("INSERT INTO LAPR2_G5.Movimentos "
                    + "(id_movimento, iban_movimento,"
                    + " montante_movimento, registo_movimento, id_origem_movimento)"
                    + "VALUES "
                    + "(?,?,?, current_timestamp(), " + id_origem + ")");
            ts.setInt(1, getIdMovimentos());
            ts.setString(2, obj.getIban_movimento());
            ts.setDouble(3, obj.getMontante_movimento());
            ts.executeUpdate();

            ts.close();
            st.close();
            stt.close();;
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
    }

    public static void insertMovimentoPagamento(int entidade, int referencia, double valor) {
        PreparedStatement st = null;
        PreparedStatement stt = null;
        Movimentos obj = new Movimentos(entidade, referencia, valor);
        int id_origem = Helper.getLoginUser().getId_utilizador();
        try {
            Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("INSERT INTO LAPR2_G5.Movimentos "
                    + "(id_movimento, referencia_movimento, entidade_movimento,"
                    + " montante_movimento, registo_movimento, id_origem_movimento)"
                    + "VALUES "
                    + "(?,?,?,?, current_timestamp(), " + id_origem + ")");
            st.setInt(1, getIdMovimentos());
            st.setInt(2, obj.getEntidade_movimento());
            st.setInt(3, obj.getReferencia_movimento());
            st.setDouble(4, obj.getMontante_movimento());
            st.executeUpdate();

            stt = conn.prepareStatement("UPDATE LAPR2_G5.Utilizador SET saldo_utilizador = saldo_utilizador - ? WHERE id_utilizador = ?");
            stt.setDouble(1, valor);
            stt.setInt(2, getId());
            stt.executeUpdate();

            st.close();
            conn.close();

        } catch (SQLException e) {
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovimentosDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
     public static void insertMovimentoDeposito(double valor, String Iban) {
        PreparedStatement st = null;
        PreparedStatement ts = null;
        Movimentos obj = new Movimentos(valor, Iban);
        int id_origem = Helper.getLoginUser().getId_utilizador();
        try {
            Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("UPDATE LAPR2_G5.Utilizador SET saldo_utilizador = saldo_utilizador + ? WHERE iban_utilizador=?");
            st.setDouble(1, valor);
            st.setString(2, Iban);
            st.executeUpdate();
            
            ts = conn.prepareStatement("INSERT INTO LAPR2_G5.Movimentos "
                    + "(id_movimento, iban_movimento,"
                    + " montante_movimento, registo_movimento, id_origem_movimento)"
                    + "VALUES "
                    + "(?,?,?, current_timestamp(), " + id_origem + ")");
            ts.setInt(1, getIdMovimentos());
            ts.setString(2, obj.getIban_movimento());
            ts.setDouble(3, obj.getMontante_movimento());
            ts.executeUpdate();

            ts.close();
            st.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
    }
    
    public static void UpdateMovimento(Movimentos obj) {
        PreparedStatement st = null;
        try {
            Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("UPDATE LAPR2_G5.Movimentos "
                    + "(SET id_movimento = ?, SET id_destino_movimento = ?, SET id_origem_movimento = ?, "
                    + "SET iban_movimento = ?, SET referencia_movimento = ?, SET entidade_movimento = ? "
                    + "SET montante_movimento = ?, SET tipo_movimento = ?, SET registo_movimento = ?)"
                    + "WHERE id_movimento = ? ");
            st.setInt(1, obj.getId_movimento());
            st.setInt(2, obj.getId_destino_movimento());
            st.setInt(3, obj.getId_origem_movimento());
            st.setString(4, obj.getIban_movimento());
            st.setInt(5, obj.getReferencia_movimento());
            st.setInt(6, obj.getEntidade_movimento());
            st.setDouble(7, obj.getMontante_movimento());
            st.setInt(8, obj.getTipo_movimento());
            st.setDate(9, (Date) obj.getRegisto_movimento());
            st.setInt(10, obj.getId_movimento());

            ResultSet rs = st.executeQuery();
            st.close();
            rs.close();
            conn.close();

        } catch (SQLException e) {
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovimentosDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void deleteMovimento(Integer id) {
        PreparedStatement st = null;
        try {
            Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("DELETE FROM LAPR2_G5.Movimentos WHERE id_movimento = ?");
            st.setInt(1, id);

            st.executeUpdate();;
            ResultSet rs = st.executeQuery();

            st.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovimentosDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Movimentos> getAllList() {
        List<Movimentos> list = new ArrayList<>();
        String sql = "SELECT * FROM LAPR2_G5.Movimentos";
        try {
            Connection conn = ConnDB.getConnDB();
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Movimentos movimento = new Movimentos();
                movimento.setId_movimento(rs.getInt("id_movimento"));
                movimento.setId_destino_movimento(rs.getInt("id_destino_movimento"));
                movimento.setId_origem_movimento(rs.getInt("id_origem_movimento"));
                movimento.setIban_movimento(rs.getString("iban_movimento"));
                movimento.setEntidade_movimento(rs.getInt("entidade_movimento"));
                movimento.setReferencia_movimento(rs.getInt("referencia_movimento"));
                movimento.setMontante_movimento(rs.getDouble("montante_movimento"));
                movimento.setTipo_movimento(rs.getInt("tipo_movimento"));
                movimento.setRegisto_movimento(rs.getDate("registo_movimento"));

                list.add(movimento);
            }
            st.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println("Ocorreu um erro:" + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovimentosDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Movimentos> getListabyIdOrigem(int id) {
        List<Movimentos> list = new ArrayList<>();
        String sql = "SELECT * FROM LAPR2_G5.Movimentos WHERE id_origem_movimento='" + id + "'";
        try {
            Connection conn = ConnDB.getConnDB();
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Movimentos movimento = new Movimentos();
                movimento.setId_movimento(rs.getInt("id_movimento"));
                movimento.setId_destino_movimento(rs.getInt("id_destino_movimento"));
                movimento.setId_origem_movimento(rs.getInt("id_origem_movimento"));
                movimento.setIban_movimento(rs.getString("iban_movimento"));
                movimento.setEntidade_movimento(rs.getInt("entidade_movimento"));
                movimento.setReferencia_movimento(rs.getInt("referencia_movimento"));
                movimento.setMontante_movimento(rs.getDouble("montante_movimento"));
                movimento.setTipo_movimento(rs.getInt("tipo_movimento"));
                movimento.setRegisto_movimento(rs.getDate("registo_movimento"));

                list.add(movimento);
            }
            st.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println("Ocorreu um erro:" + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MovimentosDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static int getIdMovimentos() throws SQLException, ClassNotFoundException {

        Connection conn = ConnDB.getConnDB();
        String cmd;
        cmd = "SELECT MAX(id_movimento) AS id_movimento FROM LAPR2_G5.Movimentos";
        int id_movimento = 0;

        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        ResultSet rs = preparedStatement.executeQuery(cmd);
        while (rs.next()) {

            id_movimento = rs.getInt("id_movimento");

        }
        preparedStatement.close();

        System.out.println("id movimento=" + (id_movimento));
        id_movimento += 1;

        return id_movimento;
    }
}

