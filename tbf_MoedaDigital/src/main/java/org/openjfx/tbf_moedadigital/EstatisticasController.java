/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Óscar Jorge
 */
public class EstatisticasController implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
     @FXML
    private void margemLucro(ActionEvent event) {

        try {
            Connection conn = ConnDB.getConnDB();
            String cmd = ("SELECT SUM(saldo_utilizador) FROM LP2_G5.Utilizador");
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);

            while (rs.next()) {
                System.out.println(cmd);
            }
            statement.close();
        } catch (Exception e) {
        }
       
    }

    @FXML
    private void clientesPendentes(ActionEvent event) throws SQLException, ClassNotFoundException {
        getNome();
        }
    public List getNClientes(){
        List ids = new ArrayList();
        
        try {
            Connection conn = ConnDB.getConnDB();
            String cmd = ("SELECT (id_utilizador_prestacao) FROM Prestacoes");
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);

            if (rs.next()) {
               ids.add(rs.getInt("id_utilizador_prestacao"));
            }
            statement.close();
        } catch (Exception e) {
        }
        return ids;  
        
    }
    public void getNome() throws SQLException, ClassNotFoundException{
        List ids = getNClientes();
         List nome = new ArrayList();
        double valorFaltaDevolver = clientes_Pendentes();
   
         for (Object id : ids){
        
            Connection conn = ConnDB.getConnDB();
            String cmd = ("SELECT nome_utilizador FROM LAPR2_G5.Utilizador WHERE id_utilizador = " +id);
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);
            
            nome.add(rs.getString("nome_utilizador"));
             statement.close();
        }
        
        Helper.AlertVazio("Valor que falta devolver ao banco devido ao empréstimo:" + valorFaltaDevolver+ nome.toString());

    }
    
    
    public double clientes_Pendentes(){
        double valorFaltaDevolver = 0;
        try {
            Connection conn = ConnDB.getConnDB();
            String cmd = ("SELECT total_prestacao, pago_prestacao FROM Prestacoes");
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);

            if (rs.next()) {
                valorFaltaDevolver += rs.getDouble("total_prestacao") - rs.getDouble("pago_prestacao");
            }
        } catch (Exception e) {
        }
        return valorFaltaDevolver;
    }

    @FXML
    private void PercentagemCrédito(ActionEvent event) {
        int totalClientesComCredito = 0;
        int totalClientes = 0;
        double calcularPercentagem = 0;
        
        try {
            Connection conn = ConnDB.getConnDB();
            String cmd = ("SELECT count(DISTINCT Prestacoes.id_utilizador_prestacao) FROM Utilizador, Prestacoes Where Prestacoes.id_utilizador_prestacao = Utilizador.id_utilizador");
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);

            if (rs.next()) {
                totalClientesComCredito = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        
        try {
            Connection conn = ConnDB.getConnDB();
            String cmd = ("SELECT count(*) FROM Utilizador Where tipo_utilizador = 2");
            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);

            if (rs.next()) {
                totalClientes = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        
        calcularPercentagem = (totalClientesComCredito * 100) / totalClientes;  
        
        Helper.AlertVazio("Percentagem de clientes registados que têm algum crédito: " + calcularPercentagem + "%");

    }

}
