package org.openjfx.tbf_moedadigital;

import java.io.IOException;
import java.net.URL;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;


/**
 * FXML Controller class
 *
 * @author Tiago
 */
public class SimuladorCreditoController implements Initializable {

    @FXML
    private Label laTipocredito;
    @FXML
    private Label laMontante;
    @FXML
    private Label laMeses;
    @FXML
    private Label laCalculo;
    @FXML
    private ComboBox<Credito> BoxTipocredito;
    @FXML
    private TextField FielMontante;
    @FXML
    private TextField FielMeses;
    @FXML
    private Label laTitulo;
    @FXML
    private Label laOrdenadoBase;
    @FXML
    private TextField FieldOrdenadoBase;
    @FXML
    private Button ButSeguinte;
    @FXML
    private Button ButAceitar;
     @FXML
    private Label laApresenta;
    /**
     * Initializes the controller class.
     */

    private static DecimalFormat df1 = new DecimalFormat("#.##");
    public static String encargosG, ordenadoG,montante,Meses,NomeCredito;
    public static double taxa;
    public static String[] ValorTaxas = new String[100];
    public static boolean condicoes = true;
    private static final int COD_LENGTH = 6;
    private static final Random RANDOM = new SecureRandom();
    private static final Scanner scan = new Scanner(System.in);
    private double mtic, var1 ,var3;
    private TaxasCreditos Taxa;

    @FXML
    private AnchorPane topPane;
   

    @Override
    public void initialize(URL url, ResourceBundle rb) {
  
        laTitulo.setText("Simulador de Crédito");
        laTipocredito.setText("Escolha o projeto");
        laMontante.setText("Escolha o montante");
        laMeses.setText("Escolha o prazo");
        laOrdenadoBase.setText("Ordenado base");
        laCalculo.setText("Aguardar");
        ButAceitar.setVisible(false);
        ComboBoxTipoCreditoApresentacao();
        laApresenta.setText("");


       
    }

    public void getTipoCredito() throws ClassNotFoundException, SQLException
    {   
        ObservableList<Credito> techno = FXCollections.observableArrayList(Credito.getCredito());
        BoxTipocredito.setItems(techno); 
        
    }
    
    
    public TaxasCreditos getTipoTaxas() throws ClassNotFoundException, SQLException
    {   
        Taxa=null;
    
       NomeCredito=BoxTipocredito.getValue().getNome_credito();
       if("Pessoal".equals(NomeCredito))
       {
           Taxa = TaxasCreditos.getTaxaCreditoPessoal();
           
       }
        if("Habitação".equals(NomeCredito))
       {
           Taxa=TaxasCreditos.getTaxaCreditoHabitacao();
       }
        if("Automóvel".equals(NomeCredito))
       {
           Taxa=TaxasCreditos.getTaxaCreditoAutomovel();
       }
       return Taxa;
    }


    @FXML
    private void ButtonValidar(ActionEvent event) throws ClassNotFoundException, SQLException {
        ordenadoG = FieldOrdenadoBase.getText();
        montante = FielMontante.getText();
        Meses = FielMeses.getText();
        getTipoTaxas();
        verificaDados();
        taxa = Double.parseDouble(getTipoTaxas().getDescricao_taxa()); 
        double OrdenadoLiquido = Double.parseDouble(ordenadoG);
        var1 = (Double.parseDouble(montante)) * (taxa/100);
        double var2 = Integer.parseInt(Meses);
        var3 = (var1+(Double.parseDouble(montante))) / var2;
        mtic=var1+(Double.parseDouble(montante));
        
        if (var3 < (OrdenadoLiquido * 0.3) && condicoes) {
            laCalculo.setText("Credito APROVADO\nA sua prestacao é: " + df1.format(var3)+"€");
            ButAceitar.setVisible(true);
            ValorTaxaApresentacao("Taxa: "+taxa+"%  "+" MTIC: "+df1.format(mtic)+"€");
        
        } else if(var3 > (OrdenadoLiquido * 0.3) && condicoes){
            laCalculo.setText("Credito REPROVADO\n ");
            Helper.showErrorAlert("Credito REPROVADO");
            ButAceitar.setVisible(false);
        }

    }
    


    @FXML
    private void ButAceitar(ActionEvent event) throws IOException, SQLException, ClassNotFoundException {
        System.out.println("#mudar de pagina #");
        int id_prestacao = getIdPrestacao();
        int id_utilizador_prestacao = Helper.getLoginUser().getId_utilizador();
        Credito cred = BoxTipocredito.getSelectionModel().getSelectedItem();
        int id_credito_prestacao = cred.getId_credito();
        int idTaxa = getTipoTaxas().getId_taxa(); 
        String codigo_prestacao_credito = generateRandomCodPrestacao();
        String periodo = FielMeses.getText();
        int periodo_prestacao = Integer.parseInt(periodo);
        LocalDate date = LocalDate.now();
        
        String cmd = "INSERT INTO `LAPR2_G5`.`Prestacoes`"
        + "(`id_prestacao`, `id_utilizador_prestacao`, `id_credito_prestacao`, `id_taxa_prestacao`, `codigo_prestacao_credito`,  `periodo_prestacao`, "
        + "`total_prestacao`, `juros_prestacao`, `pago_prestacao`, `estado_prestacao`, `registo_prestacao`, `data_inicio_prestacao`, `data_fim_prestacao`)"
        +"VALUES"
        + "('"+id_prestacao+"','"+id_utilizador_prestacao+"','"+id_credito_prestacao+"','"+idTaxa+"','"+codigo_prestacao_credito+"','"+periodo_prestacao+"',"
                + "'"+mtic+"','"+var1+"','"+var3+"','1',CURRENT_TIMESTAMP(),CURRENT_DATE(), DATE_ADD( CURRENT_DATE(), INTERVAL " +periodo_prestacao +" Month) );";
        Helper.InsertUpdateDeleteBD(cmd);
    }

    private void ComboBoxTipoCreditoApresentacao() {
                try {
            getTipoCredito();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(SimuladorCreditoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void ValorTaxaApresentacao(String texto) {
            laApresenta.setText(texto);
    }

    @FXML
    private void combooo(ActionEvent event) {
    }

    private void verificaDados() {
      if("Pessoal".equals(NomeCredito))
       {
            if ((Integer.parseInt(Meses)<6)|| Integer.parseInt(Meses)>36) {
                Helper.showInfoAlert("Apenas é possivel efetuar credito 6 a 36 meses (3 anos)");
                condicoes=false;
                laCalculo.setText("Aguardar");
                laApresenta.setText("");
        ButAceitar.setVisible(false);
            }else{
                condicoes=true;
            } 
           
       }
        if("Habitação".equals(NomeCredito))
       {
            if ((Integer.parseInt(Meses)<0)|| Integer.parseInt(Meses)>480) {
                Helper.showInfoAlert("Apenas é possivel efetuar credito até 480 meses (4 anos)");
                condicoes=false;
                laCalculo.setText("Aguardar");
                laApresenta.setText("");
        ButAceitar.setVisible(false);
            }else{
                condicoes=true;
            }   
       }
        if("Automóvel".equals(NomeCredito))
       {
            if ((Integer.parseInt(Meses)<6)|| Integer.parseInt(Meses)>36) {
                Helper.showInfoAlert("Apenas é possivel efetuar credito 6 a 36 meses (3 anos)");
                condicoes=false;
                laCalculo.setText("Aguardar");
                laApresenta.setText("");
        ButAceitar.setVisible(false);
            }else{
                condicoes=true;
            }    
       }
    }


     public int getIdPrestacao() throws SQLException, ClassNotFoundException{
        
        Connection conn = ConnDB.getConnDB();
        String cmd;
        cmd = "SELECT MAX(id_prestacao) As id_prestacao FROM LAPR2_G5.Prestacoes ";
        int id_prestacao = 0;
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            ResultSet rs = preparedStatement.executeQuery(cmd);
            while (rs.next()) {
                
                
                id_prestacao = rs.getInt("id_prestacao");
                
            }
        }
                
        System.out.println("ultimo id= "+ id_prestacao);
        id_prestacao += 1;
        System.out.println("novo id= "+(id_prestacao));

    return id_prestacao;
    }

      public static String generateRandomCodPrestacao(){

        String letters = "123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";

        String pw = "";
        for (int i=0; i<COD_LENGTH; i++){
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
    return pw;
    }
}
