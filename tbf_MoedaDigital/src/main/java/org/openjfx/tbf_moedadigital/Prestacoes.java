/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Tiago
 */
public class Prestacoes {
    
    
    private Integer id_prestacao;
    private Integer id_utilizador_prestacao;
    private Integer id_credito_prestacao;
    private Integer id_taxa_prestacao;
    private String codigo_prestacao_credito;
    private Integer periodo_prestacao;
    private Double total_prestacao;
    private Double juros_prestacao;
    private Double pago_prestacao;
    private Integer estado_prestacao;
    private Date registo_prestacao;
    private Date data_inicio_prestacao;
    private Date data_fim_prestacao;
 
    public Prestacoes(){
    }

    public Prestacoes(Integer id_prestacao, Integer id_utilizador_prestacao, Integer id_credito_prestacao, Integer id_taxa_prestacao, 
            String codigo_prestacao_credito, Integer periodo_prestacao, Double total_prestacao, Double juros_prestacao, 
            Double pago_prestacao, Integer estado_prestacao, Date registo_prestacao, 
            Date data_inicio_prestacao, Date data_fim_prestacao) 
    {
        this.id_prestacao = id_prestacao;
        this.id_utilizador_prestacao = id_utilizador_prestacao;
        this.id_credito_prestacao = id_credito_prestacao;
        this.id_taxa_prestacao=id_taxa_prestacao;
        this.codigo_prestacao_credito=codigo_prestacao_credito;
        this.periodo_prestacao=periodo_prestacao;
        this.total_prestacao=total_prestacao;
        this.juros_prestacao=juros_prestacao;
        this.pago_prestacao=pago_prestacao;
        this.estado_prestacao=estado_prestacao;
        this.registo_prestacao=registo_prestacao;
        this.data_inicio_prestacao=data_inicio_prestacao;
        this.data_fim_prestacao=data_fim_prestacao;

    }

    public Integer getId_prestacao() {
        return id_prestacao;
    }

    public void setId_prestacao(Integer id_prestacao) {
        this.id_prestacao = id_prestacao;
    }

      public Integer getId_utilizador_prestacao() {
        return id_utilizador_prestacao;
    }

    public void setId_utilizador_prestacao(Integer id_utilizador_prestacao) {
        this.id_utilizador_prestacao = id_utilizador_prestacao;
    }
          
    public Integer getId_credito_prestacao() {
        return id_credito_prestacao;
    }

    public void setId_credito_prestacao(Integer id_credito_prestacao) {
        this.id_credito_prestacao = id_credito_prestacao;
    } 
    
    public Integer getId_taxa_prestacaoo() {
        return id_taxa_prestacao;
    }

    public void setId_taxa_prestacao(Integer id_taxa_prestacao) {
        this.id_taxa_prestacao = id_taxa_prestacao;
    }    
    
     public String getCodigo_prestacao_credito() {
        return codigo_prestacao_credito;
    }

    public void setCodigo_prestacao_credito(String codigo_prestacao_credito) {
        this.codigo_prestacao_credito = codigo_prestacao_credito;
    }
    
    public Integer getPeriodo_prestacao() {
        return id_prestacao;
    }

    public void setPeriodo_prestacao(Integer periodo_prestacao) {
        this.periodo_prestacao = periodo_prestacao;
    }    
    
    public Double getTotal_prestacao() {
        return total_prestacao;
    }

    public void setTotal_prestacao(Double total_prestacao) {
        this.total_prestacao = total_prestacao;
    }    
    
    public Double getJuros_prestacao() {
        return juros_prestacao;
    }

    public void setJuros_prestacao(Double juros_prestacao) {
        this.juros_prestacao = juros_prestacao;
    }    
   
    public Double getPago_prestacao() {
        return pago_prestacao;
    }

    public void setPago_prestacao(Double pago_prestacao) {
        this.pago_prestacao = pago_prestacao;
    }        
    
    public Integer getEstado_prestacao() {
        return estado_prestacao;
    }

    public void setEstado_prestacao(Integer estado_prestacao) {
        this.estado_prestacao = estado_prestacao;
    }

    public Date getRegisto_prestacao() {
        return registo_prestacao;
    }

    public void setRegisto_prestacao(Date registo_prestacao) {
        this.registo_prestacao = registo_prestacao;
    }    
    

    public Date getData_inicio_prestacao() {
        return data_inicio_prestacao;
    }

    public void setData_inicio_prestacao(Date data_inicio_prestacao) {
        this.data_inicio_prestacao = data_inicio_prestacao;
    }    
    

    public Date getData_fim_prestacao() {
        return data_fim_prestacao;
    }

    public void setData_fim_prestacao(Date data_fim_prestacao) {
        this.data_fim_prestacao = data_fim_prestacao;
    }    
    

    @Override
    public String toString() {
        return "Prestacoes{" + "id_utilizador_prestacao=" + id_utilizador_prestacao + ", id_credito_prestacao=" + id_credito_prestacao + ", registo_prestacao=" + registo_prestacao + ", total_prestacao=" + total_prestacao +'}';
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prestacoes other = (Prestacoes) obj;
        if (!Objects.equals(this.id_utilizador_prestacao, other.id_utilizador_prestacao)) {
            return false;
        }
        return true;
    }  
    
}