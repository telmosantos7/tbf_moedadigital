package org.openjfx.tbf_moedadigital;

import static java.lang.Math.random;
import java.net.URL;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class NovoClienteController implements Initializable {


    @FXML
    private TextField txtFNome;
    @FXML
    private TextField txtFNIF;
    @FXML
    private TextField txtFIBAN;
    @FXML
    private TextField txtFMorada;
    @FXML
    private TextField txtFEmail;
    @FXML
    private ComboBox<Moeda> combBoxMoeda;
    @FXML
    private ComboBox<TipoUtilizador> combBoxTPUser;
    @FXML
    private AnchorPane NovoClienteScene;
    
    //atributos
    private int id_utilizador;
    private String nome_utilizador;
    private String nif_utilizador;
    private String iban_utilizador;
    private String morada_utilizador;
    private String email_utilizador;
    private int utilizador_id_moeda;
    private int tipo_utilizador;
    private static final Random RANDOM = new SecureRandom();
    private static final int PASSWORD_LENGTH = 15;
    private static final int USER_LENGTH = 10;
    private String codigo_acesso_utilizador;
    private String utilizador_num_contrato;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        verificarInputText();
        try { 
            TipoUtilizador();
            MoedaPreferencial();         
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(NovoClienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    @FXML
    private void bttnGravar(ActionEvent event) throws SQLException, ClassNotFoundException, Exception {        
        boolean blank = txtFNome.getText().isBlank() || txtFIBAN.getText().isBlank() || txtFMorada.getText().isBlank() || txtFNIF.getText().isBlank() || txtFNome.getText().isBlank() || txtFEmail.getText().isBlank();
        if(!blank){
            gravarNovoUser();
        }else{
            Helper.showErrorAlert("Campos obrigatórios por preencher!");
        }
        
    }
    
    @FXML
    private void bttnSair(ActionEvent event) throws SQLException, ClassNotFoundException {
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("TBF Client Management");
        alert.setHeaderText("AVISO: Grave o cliente antes de sair!");
        alert.setContentText("Deseja mesmo sair?");
        
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");
        alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO); 
        
        Optional<ButtonType> result = alert.showAndWait();        
        if (result.get() == buttonTypeYES) {
            fechar();
        } else {
            event.consume();

        }
    
    }
    private void verificarInputText(){
        
        txtFNome.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFNome.getText().length() > 32) {
                    txtFNome.deleteNextChar();
                    Helper.showInfoAlert("O campo Nome não pode exceder 32 caracteres.");
                }
            }
        });
            txtFNIF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFNIF.getText().length() > 9) {
                    txtFNIF.deleteNextChar();
                    Helper.showInfoAlert("O campo NIF não pode exceder 9 caracteres.");
                }
            }
        });
            txtFIBAN.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFIBAN.getText().length() > 30) {
                    txtFIBAN.deleteNextChar();
                    Helper.showInfoAlert("O campo IBAN não pode exceder 30 caracteres.");
                }
            }
        });
            txtFMorada.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFMorada.getText().length() > 50) {
                    txtFMorada.deleteNextChar();
                    Helper.showInfoAlert("O campo Morada não pode exceder 50 caracteres.");
                }
            }
        });
            txtFEmail.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFEmail.getText().length() > 100 ) {
                    txtFEmail.deleteNextChar();
                    Helper.showInfoAlert("O campo Email não pode exceder 100 caracteres.");
                }
            }
        });
    }
    private void gravarNovoUser() throws SQLException, ClassNotFoundException, Exception {

            id_utilizador = getIdUtilizador() + 1;  //set id next ao ultimo id da bd
            nome_utilizador = txtFNome.getText();
            nif_utilizador = txtFNIF.getText();
            iban_utilizador = txtFIBAN.getText();
            morada_utilizador = txtFMorada.getText();
            email_utilizador = txtFEmail.getText();
            codigo_acesso_utilizador = generateRandomCodigoAcesso();
            utilizador_num_contrato = generateRandomUtilizadorNumContrato();
            utilizador_id_moeda = getIdMoedaSelecionada();
            tipo_utilizador = getTipoUtilizadorSelecionado();
            
        String cmd = "INSERT INTO LAPR2_G5.Utilizador\n" +
                "(id_utilizador, utilizador_num_contrato, codigo_acesso_utilizador, email_utilizador, nome_utilizador, nif_utilizador, \n" +
                "morada_utilizador, utilizador_id_moeda, saldo_utilizador, tipo_utilizador, registo_utilizador, iban_utilizador)\n" +
                "VALUES\n" +
                "('" +id_utilizador + "','"+utilizador_num_contrato+"','"+codigo_acesso_utilizador+"','"+ email_utilizador + "','" 
                    +nome_utilizador+ "','"+ nif_utilizador+"','"+morada_utilizador+"','"+utilizador_id_moeda+"','0','"+tipo_utilizador+"',CURRENT_TIMESTAMP,'"+iban_utilizador+"');";

        Helper.InsertUpdateDeleteBD(cmd);
        Helper.showAlertGravar("Cliente/Admin gravado com sucesso");
        sendEmail.sendMail(email_utilizador,"Dados de Login TBF", nome_utilizador + " os seus dados de acesso são: \nUser: " +utilizador_num_contrato+"\nPassword: "+ codigo_acesso_utilizador);
        fechar();
        }
    
    private void MoedaPreferencial() throws ClassNotFoundException, SQLException {
        
        // tipos de moeda
        ObservableList<Moeda> techno = FXCollections.observableArrayList(Moeda.getLista());
        combBoxMoeda.setItems(techno);          
    }
    private void TipoUtilizador() throws ClassNotFoundException, SQLException{
        // tipo utilizador
        ObservableList<TipoUtilizador> nn = FXCollections.observableArrayList(TipoUtilizador.getLista());
        combBoxTPUser.setItems(nn);
    }
    
    @FXML
    private void combBoxMoeda_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException {

    }

    @FXML
    private void combBoxTPUser_OnAction(ActionEvent event) throws ClassNotFoundException {

    }
    
    public static String generateRandomCodigoAcesso(){

        String letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ123456789";

        String pw = "";
        for (int i=0; i<PASSWORD_LENGTH; i++){
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
    return pw;
    }
    
    public static String generateRandomUtilizadorNumContrato(){

        String letters = "123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";

        String pw = "";
        for (int i=0; i<USER_LENGTH; i++){
            int index = (int)(RANDOM.nextDouble()*letters.length());
            pw += letters.substring(index, index+1);
        }
    return pw;
    }
    
    public int getIdUtilizador() throws SQLException, ClassNotFoundException{
        
        Connection conn = ConnDB.getConnDB();
        String cmd;
        cmd = "SELECT MAX(id_utilizador) AS id_utilizador FROM LAPR2_G5.Utilizador";
        TipoUtilizador item = new TipoUtilizador();
        
        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        ResultSet rs = preparedStatement.executeQuery(cmd);
         while (rs.next()) {
                    
                    
                    item.setTipo_utilizador(rs.getInt("id_utilizador"));
                    
                }
                preparedStatement.close();
                
        id_utilizador = item.getTipo_utilizador();
        System.out.println("id user="+(id_utilizador +1));

    return id_utilizador;
    }
    
    public int getIdMoedaSelecionada() throws SQLException, ClassNotFoundException{
        
        Moeda moedaPreferencial = combBoxMoeda.getValue();
        int id_moeda = 0;

        if(moedaPreferencial == null){ id_moeda = 1; }
        else {
            id_moeda = combBoxMoeda.getValue().getId_moeda();
        }
       
    return id_moeda;
    }
    
    public int getTipoUtilizadorSelecionado(){
        
        TipoUtilizador tipo;
        tipo = combBoxTPUser.getValue(); // recebe tipo_utilizador
        
        int tipo_utilizador;
        
        if(tipo == null){ tipo_utilizador = 2; } // deafult utilizador tipo Cliente
        else{ tipo_utilizador = combBoxTPUser.getValue().getTipo_utilizador(); } // utilizador escolhido sendo este 1-Admin ou 2-Cliente
        
    return tipo_utilizador;
    }
    
    public void fechar(){
        Window window = NovoClienteScene.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}