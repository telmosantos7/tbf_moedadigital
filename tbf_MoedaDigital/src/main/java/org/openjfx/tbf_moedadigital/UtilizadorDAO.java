/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Pc
 */
public class UtilizadorDAO {

    public static Utilizadores getUtilizadorById(String id) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conn = ConnDB.getConnDB();
        String cmd;
        Utilizadores obj = new Utilizadores();
        try {
            cmd = "SELECT * FROM LAPR2_G5.Utilizador WHERE utilizador_num_contrato='" + id + "'";

            try (PreparedStatement statement = conn.prepareStatement(cmd)) {
                System.out.println(id);
                ResultSet rs = statement.executeQuery(cmd);
                
                System.out.println("rs.getInt(\"id_utilizador\")");
                while (rs.next()) {
                    obj = new Utilizadores(rs.getInt("id_utilizador"), rs.getString("utilizador_num_contrato"), rs.getString("codigo_acesso_utilizador"), rs.getString("iban_utilizador"), rs.getString("nome_utilizador"));
                }
            }
        } catch (SQLException ex) {
            System.err.println("Erro: " + ex.getMessage());
        }
        return obj;
    }

    public static double getSaldo() {
        Utilizadores obj = Helper.getLoginUser();
        PreparedStatement st = null;
        try {
           Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("SELECT * FROM LAPR2_G5.Utilizador WHERE id_utilizador= ?");
            st.setInt(1, obj.getId_utilizador());
            ResultSet rs = st.executeQuery();
            
            while (rs.next()){
                obj.setSaldo_utilizador(rs.getDouble("saldo_utilizador"));
                obj.setId_utilizador(rs.getInt("id_utilizador"));
            }
            
            st.close();
            rs.close();
            conn.close();
            
            System.out.println(obj);
   
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }

        return obj.getSaldo_utilizador();
    }
    
     public static int getId() {
        Utilizadores obj = Helper.getLoginUser();
        PreparedStatement st = null;
        try {
           Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("SELECT * FROM LAPR2_G5.Utilizador WHERE id_utilizador= ?");
            st.setInt(1, obj.getId_utilizador());
            ResultSet rs = st.executeQuery();
            
            while (rs.next()){
                obj.setId_utilizador(rs.getInt("id_utilizador"));
            }
            
            st.close();
            rs.close();
            conn.close();
            
            System.out.println(obj);
   
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }

        return obj.getId_utilizador();
    }
    
    public static void trasnferencia(double valor, String Iban){
         PreparedStatement st = null;
         PreparedStatement stt = null;
        try {
           Connection conn = ConnDB.getConnDB();
            st = conn.prepareStatement("UPDATE LAPR2_G5.Utilizador SET saldo_utilizador = saldo_utilizador + ? WHERE iban_utilizador=?");
            st.setDouble(1, valor);
            st.setString(2, Iban);
            st.executeUpdate();
            
            stt = conn.prepareStatement("UPDATE LAPR2_G5.Utilizador SET saldo_utilizador = saldo_utilizador - ? WHERE id_utilizador = ?");
            stt.setDouble(1, valor);
            stt.setInt(2, getId());
            stt.executeUpdate();
   
            
            st.close();
            conn.close();
   
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
    }
   
}
