package org.openjfx.tbf_moedadigital;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException, Exception {

        scene = new Scene(loadFXML("Login")); // Login pag
        //scene = new Scene(loadFXML("GestaoMoeda")); // Login page
        //scene = new Scene(loadFXML("GestaoClientes")); // para testes GestaoClientes page
        //scene = new Scene(loadFXML("GestaoMoeda"));// para testes GestaoMoeada page
        //scene = new Scene(loadFXML("Estatisticas"));
        //scene = new Scene(loadFXML("GestaoClientes"));
        //scene = new Scene(loadFXML("MenuCliente"));
        //scene = new Scene(loadFXML("ConsultaSaldoMovimento"));
        //scene = new Scene(loadFXML("Agenda"));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("TBF Bank Management");

        Image anotherIcon = new Image("file:TBF.png");
        stage.getIcons().add(anotherIcon);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
