/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bruno
 */
public class TipoUtilizador {

    public static final int Admin = 1;
    public static final int Cliente = 2;
    private int tipo_utilizador;
    private String descricao;
    public final String SQL_SELECT = "SELECT tipo_utilizador, descricao FROM LAPR2_G5.TipoUtilizador FROM LAPR2_G5.TipoUtilizador";
    
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public int getTipo_utilizador() {
        return tipo_utilizador;
    }

    public void setTipo_utilizador(int tipo_utilizador) {
        this.tipo_utilizador = tipo_utilizador;
    }
    
     public static ArrayList<TipoUtilizador> getLista() throws ClassNotFoundException, SQLException {
            return getLst("");
        }
    public static ArrayList<TipoUtilizador> getLista(String filter) throws ClassNotFoundException, SQLException {
            return getLst(filter);
        }
        /**
         * Retorna uma lista do tipo Tipos com base na informação que é retornada da base de dados
         * @param filter
         * @return 
         */
    private static ArrayList<TipoUtilizador> getLst(String filter) throws ClassNotFoundException, SQLException {
        ArrayList<TipoUtilizador> lst1 = new ArrayList();
            
        Connection conn = ConnDB.getConnDB();
        String cmd = ("SELECT tipo_utilizador, descricao FROM LAPR2_G5.TipoUtilizador");

            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);

                while (rs.next()) {
                    
                    TipoUtilizador item = new TipoUtilizador();
                    item.setTipo_utilizador(rs.getInt("tipo_utilizador"));
                    item.setDescricao(rs.getString("descricao"));

                    lst1.add(item);
                }
                preparedStatement.close();
        return lst1;
    } 
        @Override
    public String toString() {
        return this.descricao;
    }
}
    