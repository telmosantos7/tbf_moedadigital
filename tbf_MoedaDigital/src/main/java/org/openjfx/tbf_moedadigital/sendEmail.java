
package org.openjfx.tbf_moedadigital;


import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;






public class sendEmail {
    


    public static void sendMail(String recepient, String subject, String text) throws Exception{
        System.out.println("Preparing to Send email");
       Properties properties = new Properties();
        
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smpt.port","587");
        
       String myAccountEmail = "tbfmoedadigital@gmail.com";
       String password = "tbfBanco123";
        
       Session session = Session.getInstance(properties, new Authenticator(){
       
           @Override
        protected PasswordAuthentication getPasswordAuthentication(){
           return new PasswordAuthentication(myAccountEmail,password);
        }
    });
        Message message = prepareMessage(session, myAccountEmail,recepient, subject, text);
        
        Transport.send(message);
        System.out.println("Message sent sucessfuly");
    }
    private static Message prepareMessage(Session session,String myAccountEmail, String recepient, String subject, String text){
            try{
               Message message = new MimeMessage(session);
               message.setFrom(new InternetAddress(myAccountEmail));
               message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
               message.setSubject(subject);
               message.setText(text);
                return message;
               
            }catch(Exception ex){
                Logger.getLogger(sendEmail.class.getName()).log(Level.SEVERE, null,ex);
            }
            return null;
}
}

//  public static void main(String[] args) throws MessagingException {
//        String recipient = "recipient@gmail.com";
//        String sender = "sender@gmail.com";
//        String host = "localhost";
//
//        Properties properties = System.getProperties();
//        properties.setProperty("mail.smtp.host", host);
//
//        Session session = Session.getDefaultInstance(properties);
//
//        try {
//            MimeMessage message = new MimeMessage(session);
//            message.setFrom(new InternetAddress(sender));
//            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
//            message.setSubject("Receipt for your product!");
//
//            BodyPart messageBodyPart = new MimeBodyPart();
//            messageBodyPart.setText("Thank you for buying with us, here's a receipt for your product.");
//
//            Multipart multipart = new MimeMultipart();
//            multipart.addBodyPart(messageBodyPart);
//
//            messageBodyPart = new MimeBodyPart();
//            DataSource source = new FileDataSource("receipt.txt");
//            multipart.addBodyPart(messageBodyPart);
//
//            message.setContent(multipart);
//
//            Transport.send(message);
//
//        } catch (AddressException e) {
//            e.printStackTrace();
//        }
//    }

//
//
////
