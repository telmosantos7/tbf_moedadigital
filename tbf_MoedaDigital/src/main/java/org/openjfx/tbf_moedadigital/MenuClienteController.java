/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class MenuClienteController implements Initializable {


    @FXML
    private AnchorPane userPNG;
    @FXML
    private Label lblUser;
    @FXML
    private Button btLogOff;
    @FXML
    private Button btn_SimularCredito;
    @FXML
    private Button btn_Pagamentos;
    @FXML
    private Button btn_Saldo_Mov;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblUser.setText(Helper.getLoginUser().getNome_utilizador());
    }    
    
    @FXML
    private void btLogOff_OnAction(ActionEvent event) {
         try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Login.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            Image anotherIcon = new Image("file:TBF.png");
            stage.setResizable(false);
            stage.getIcons().add(anotherIcon);
            stage.show();
            fechar();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
        finally{ fechar(); }
    }


    @FXML
    private void btn_SimularCredito_OnAction(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SimuladorCredito.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void btn_Pagamentos_OnAction(ActionEvent event) {
           try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PagamentosServicos.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Services");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
        
    }

    @FXML
    private void btn_Saldo_Mov_OnAction(ActionEvent event) {
       try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SaldoMovimentoCliente.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }
    
    public void fechar(){
        Window window = userPNG.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
    
}
