/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Tiago
 */
public class AgendaController implements Initializable {

    Prestacoes myLista = new Prestacoes();
    @FXML
    private Button IdValidarData;
    Utilizadores uti = new Utilizadores();
    @FXML
    private TableColumn<Prestacoes, Integer> ColumnIdCliente;
    @FXML
    private TableColumn<Prestacoes, String> ColumnIdCredito;
    @FXML
    private TableColumn<Prestacoes, Date> ColumnDataPrestacao;
    @FXML
    private TableColumn<Prestacoes, Double> ColumnTotalPrestacao;
    @FXML
    private TableView TableViewDesc;
    private ObservableList<Prestacoes> ObservaList;
    List<Prestacoes> lista = new ArrayList<>();
    List<Prestacoes> listaMeses = new ArrayList<>();
    public static String[] meses = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
        "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
    ObservableList<String> ObsOptions = FXCollections.observableArrayList(meses);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        

    }

    @FXML
    private void validarData(ActionEvent event) {
        
        try {
        System.out.println("button ok");
        lista = PrestacoesDao.getPrestacoes();
        ObservaList = FXCollections.observableArrayList(lista);
        for (Prestacoes x : ObservaList) {
            System.out.println(x);
        }

        ColumnIdCliente.setCellValueFactory(new PropertyValueFactory<>("id_utilizador_prestacao"));
        ColumnIdCredito.setCellValueFactory(new PropertyValueFactory<>("id_credito_prestacao"));
        ColumnDataPrestacao.setCellValueFactory(new PropertyValueFactory<>("registo_prestacao"));
        ColumnTotalPrestacao.setCellValueFactory(new PropertyValueFactory<>("total_prestacao"));
        TableViewDesc.setItems(ObservaList);

        }
        catch (SQLException e){
               e.printStackTrace();
            }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }

    }

}
