/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Tiago
 */
public class TaxasCreditos {
    
    private int id_taxa;
    String descricao_taxa;
    private String nome_taxa;
    
    public TaxasCreditos(){
    }

    public TaxasCreditos(int id_taxa, String descricao_taxa, String nome_taxa) {
        this.id_taxa = id_taxa;
        this.descricao_taxa = descricao_taxa;
        this.nome_taxa = nome_taxa;
    }

    public int getId_taxa() {
        return id_taxa;
    }

    public void setId_taxa(int id_taxa) {
        this.id_taxa = id_taxa;
    }

    public String getDescricao_taxa() {
        return descricao_taxa;
    }

    public void setDescricao_taxa(String descricao_taxa) {
        this.descricao_taxa = descricao_taxa;
    }

    public String getNome_taxa() {
        return nome_taxa;
    }

    public void setNome_taxa(String nome_credito) {
        this.nome_taxa = nome_taxa;
    }
    
        public static TaxasCreditos getTaxaCreditoPessoal() throws ClassNotFoundException, SQLException  {
        
         
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT id_taxa, descricao_taxa, nome_taxa FROM LAPR2_G5.Taxas WHERE id_taxa=2";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);
            TaxasCreditos item = new TaxasCreditos();

                while (rs.next()) {
                    
                    
                    item.setId_taxa(rs.getInt("id_taxa"));
                    item.setDescricao_taxa(rs.getString("descricao_taxa"));
                    item.setNome_taxa(rs.getString("nome_taxa"));
                }
                preparedStatement.close();
                
            
            return item;
    }
        public static TaxasCreditos getTaxaCreditoAutomovel() throws ClassNotFoundException, SQLException  {
        
         
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT id_taxa, descricao_taxa, nome_taxa FROM LAPR2_G5.Taxas WHERE id_taxa=3";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);
            TaxasCreditos item = new TaxasCreditos();

                while (rs.next()) {
                    
                    
                    item.setId_taxa(rs.getInt("id_taxa"));
                    item.setDescricao_taxa(rs.getString("descricao_taxa"));
                    item.setNome_taxa(rs.getString("nome_taxa"));
                }
                preparedStatement.close();
                
            
            return item;
    }
        public static TaxasCreditos getTaxaCreditoHabitacao() throws ClassNotFoundException, SQLException  {
        
         
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT id_taxa, descricao_taxa, nome_taxa FROM LAPR2_G5.Taxas WHERE id_taxa=1";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);
            TaxasCreditos item = new TaxasCreditos();

                while (rs.next()) {
                    
                    
                    item.setId_taxa(rs.getInt("id_taxa"));
                    item.setDescricao_taxa(rs.getString("descricao_taxa"));
                    item.setNome_taxa(rs.getString("nome_taxa"));
                }
                preparedStatement.close();
                
            
            return item;
    }
@Override
    public String toString() {
        return this.descricao_taxa;
    }    
}
