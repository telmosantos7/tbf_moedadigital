/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

public class Estatistica {
    double margemLucro = 0;
    int totaisPendentesC = 0;
    double percentCreditos = 0;

    public double getMargemLucro() {
        return margemLucro;
    }

    public void setMargemLucro(double margemLucro) {
        this.margemLucro = margemLucro;
    }

    public int getTotaisPendentesC() {
        return totaisPendentesC;
    }

    public void setTotaisPendentesC(int totaisPendentesC) {
        this.totaisPendentesC = totaisPendentesC;
    }

    public double getPercentCreditos() {
        return percentCreditos;
    }

    public void setPercentCreditos(double percentCreditos) {
        this.percentCreditos = percentCreditos;
    }    
}
