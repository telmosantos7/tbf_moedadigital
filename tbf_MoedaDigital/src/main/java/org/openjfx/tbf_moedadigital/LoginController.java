package org.openjfx.tbf_moedadigital;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class LoginController implements Initializable {

    @FXML
    private  TextField txtUser;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Button btnLogin; 
            
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bindForm();
    }

    @FXML
    private void txtPassword_OnKeyReleased(KeyEvent event) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        if (event.getCode() == KeyCode.ENTER) {
            login();
        }
    }

    @FXML
    private void btnLogin_click(ActionEvent event) throws IOException, SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        login();

    }
    
    @FXML
    private void receberPassword(MouseEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("RecuperarPassword.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }

    @FXML
    private void registClick(MouseEvent event) throws IOException {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Registar.fxml"));
            Parent root = loader.load();

            Scene scn = new Scene(root);
            //  scn.getStylesheets().add("/styles/Styles.css");
            Stage stg = (Stage) btnLogin.getScene().getWindow();
            stg.setTitle("TBF Client Management");
            stg.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stg.getIcons().add(anotherIcon);
            stg.setScene(scn);
            stg.show();
        
    }

    private void bindForm() {
        txtUser.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtUser.getText().length() > 32) {
                txtUser.deleteNextChar();
                Helper.showInfoAlert("O campo Username não pode exceder 32 caracteres.");
            }

            if (!newValue.matches("^[a-zA-Z0-9]+$")) {
                txtUser.deleteNextChar();
            }
        });

        txtPassword.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtPassword.getText().length() > 512) {
                txtPassword.deleteNextChar();
                Helper.showInfoAlert("O campo Password não pode exceder os 512 caracteres.");
            }
        });
    }

    public void login() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
     
        if (validarLogin()) {
            
            if(tipoUser() == 1){
                
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("MenuAdmin.fxml"));
                Parent root = loader.load();

                Scene scn = new Scene(root);
                //  scn.getStylesheets().add("/styles/Styles.css");
                Stage stg = (Stage) btnLogin.getScene().getWindow();
                stg.setTitle("TBF Management");
                stg.setResizable(false);
                Image anotherIcon = new Image("file:TBF.png");
                stg.getIcons().add(anotherIcon);
                stg.setScene(scn);
                stg.show();
                
            }else {
            
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("MenuCliente.fxml"));
                Parent root = loader.load();

                Scene scn = new Scene(root);
                //  scn.getStylesheets().add("/styles/Styles.css");
                Stage stg = (Stage) btnLogin.getScene().getWindow();
                stg.setTitle("TBF Client Area");
                stg.setResizable(false);
                Image anotherIcon = new Image("file:TBF.png");
                stg.getIcons().add(anotherIcon);
                stg.setScene(scn);
                stg.show();
            }
        }
    }

    public boolean validarLogin() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        String utilizador_num_contrato = txtUser.getText();
        String password_utilizador = txtPassword.getText();

        Utilizadores user = UtilizadorDAO.getUtilizadorById(utilizador_num_contrato);

        if (user.getId_utilizador() != 0 && user.getPassword_utilizador().equals(password_utilizador)) {
            Helper.setLoginUser(user);
            return true;
        } else {
            Helper.showErrorAlert("Nome de Utilizador ou Password inválido(a).");
            return false;
        }

    }
    public int tipoUser() throws SQLException, ClassNotFoundException {
        
        int user = 0;
        
        String utilizador_num_contrato = txtUser.getText();
        
        Connection conn = ConnDB.getConnDB();
        String cmd;
        cmd = "SELECT tipo_utilizador FROM LAPR2_G5.Utilizador WHERE utilizador_num_contrato = '"+utilizador_num_contrato+"'";
        TipoUtilizador item = new TipoUtilizador();
        
        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        ResultSet rs = preparedStatement.executeQuery(cmd);
         while (rs.next()) {
                    
                    
                    item.setTipo_utilizador(rs.getInt("tipo_utilizador"));
                    
                }
                preparedStatement.close();
    
        user = item.getTipo_utilizador();
     
    return user;
    }
}



