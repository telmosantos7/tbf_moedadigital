/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 *
 * @author lucas
 */
public class SaldoMovimentoClienteController implements Initializable {

    MovimentosDao mvtDao = new MovimentosDao();

    Utilizadores uti = new Utilizadores();
    
    @FXML
    private TableView tableViewMovimentos;

    @FXML
    private TableColumn<Movimentos, Integer> tableColumnEntidade;

    @FXML
    private TableColumn<Movimentos, Date> tableColumnData;

    @FXML
    private TableColumn<Movimentos, Integer> tableColumnReferencia;

    @FXML
    private TableColumn<Movimentos, Double> tableColumnValor;
    
    @FXML
    private TableColumn<Movimentos, String> tableColumnIban;

    @FXML
    private TextField txtSaldo;

    @FXML
    private Button btVoltar;

    @FXML
    private ObservableList<Movimentos> obsList;

    List<Movimentos> list = new ArrayList<>();

    @FXML
    public void onBtVoltarAction() {
        fechar();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initializeNodes();
    }

    public void initializeNodes() {
        uti = Helper.getLoginUser();
        int id = uti.getId_utilizador();
        System.out.println(id);
        list = mvtDao.getListabyIdOrigem(id);
        obsList = FXCollections.observableArrayList(list);
        tableColumnEntidade.setCellValueFactory(new PropertyValueFactory<>("entidade_movimento"));
        tableColumnData.setCellValueFactory(new PropertyValueFactory<>("registo_movimento"));
        tableColumnReferencia.setCellValueFactory(new PropertyValueFactory<>("referencia_movimento"));
        tableColumnValor.setCellValueFactory(new PropertyValueFactory<>("montante_movimento"));
        tableColumnIban.setCellValueFactory(new PropertyValueFactory<>("iban_movimento"));
        tableViewMovimentos.setItems(obsList);
        double x = UtilizadorDAO.getSaldo();
        txtSaldo.setText(String.format("%.2f", x));
    }

      public void fechar(){
       Stage stage = (Stage) btVoltar.getScene().getWindow();
       stage.close();
    }
}

