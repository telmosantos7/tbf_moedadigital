/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author lucas
 */
public class DepositoController {

    @FXML
    private Button btnDepositar;

    @FXML
    private Button btnVoltar;

    @FXML
    private TextField txtIban;

    @FXML
    private TextField txtValor;

    public void onBtnDepositarAction() {
        String iban = txtIban.getText();
        double valor = Double.parseDouble(txtValor.getText());
        MovimentosDao.insertMovimentoDeposito(valor, iban);
        Helper.showInfoAlert("Deposito efetuado com sucesso!");

    }

    public void onBtnVoltarAction() {
        fechar();
    }

    public void fechar() {
        Stage stage = (Stage) btnVoltar.getScene().getWindow();
        stage.close();
    }
}
