/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author Óscar Jorge
 */
public class GestaoMoedaController implements Initializable {

    @FXML
    private TextField textboxinserir;
    @FXML
    private TextField textboxCod;
    @FXML
    private TextField textboxRmoeda;
    @FXML
    private TextField textboxtax;
    @FXML
    private TableView<Moeda> tableview;
    @FXML
    private TableColumn<Moeda, Integer> id;
    @FXML
    private TableColumn<Moeda, String> descricao;
    @FXML
    private TableColumn<Moeda, String> cod;
    @FXML
    private TableColumn<Moeda, Double> tax;
   
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            vertudo();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(GestaoMoedaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void buttonIMoedasss(ActionEvent event) throws ClassNotFoundException {

        String descricao = textboxinserir.getText();
        String cod_moeda = textboxCod.getText();
        double taxa = Double.parseDouble(textboxtax.getText());

        PreparedStatement st = null;
        PreparedStatement lastID = null;
        String cmd;
        //String cmd2 ;
        try {
            Connection conn = ConnDB.getConnDB();

            
            cmd = "SELECT MAX(id_moeda) AS id_moeda FROM LAPR2_G5.Moeda;";
            PreparedStatement preparedStatement = conn.prepareStatement(cmd, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs1 = preparedStatement.getGeneratedKeys();
            rs1 = preparedStatement.executeQuery(cmd);
            rs1.next();   //get ultimo id da bd

            st = conn.prepareStatement("INSERT INTO LAPR2_G5.Moeda "
                    + "(id_moeda, descricao_moeda, codigo_moeda, taxa_cambio) "
                    + "VALUES "
                    + "(?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);

            st.setInt(1, rs1.getInt("id_moeda") + 1);
            st.setString(2, descricao);
            st.setString(3, cod_moeda);
            st.setDouble(4, taxa);

            int rowsAffected = st.executeUpdate();

            if (rowsAffected > 0) {
                ResultSet rs = st.getGeneratedKeys();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    System.out.println("Acabou! Id: " + id);
                }
            } else {
                System.out.println("Linhas não afetadas!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
//			ConnDB.closeStatement(st);
//			ConnDB.closeConnection();
        }
    }

    @FXML
    private void buttonRMoedasss(ActionEvent event) throws ClassNotFoundException {

        String cmd;
        //String cmd2 ;

        int id = Integer.parseInt(textboxRmoeda.getText());
        PreparedStatement st = null;

        try {
            Connection conn = ConnDB.getConnDB();
            
            st = conn.prepareStatement("DELETE FROM LAPR2_G5.Moeda "
                    + "WHERE "
                    + "(id_moeda) = ?");
            st.setInt(1, id);

            int rowsAffected = st.executeUpdate();

            System.out.println("Acabado! nº Ids apagados: " + rowsAffected);
        } catch (SQLException e) {

        }
    }
    public void vertudo() throws ClassNotFoundException, SQLException{
         ArrayList<Moeda> lst = Moeda.getLista();
         ObservableList<Moeda> obslst = FXCollections.observableArrayList(lst);
         tableview.setItems(obslst);
         id.setCellValueFactory(new PropertyValueFactory("id_moeda"));
         descricao.setCellValueFactory(new PropertyValueFactory("descricao_moeda"));
         cod.setCellValueFactory(new PropertyValueFactory("codigo_moeda"));
         tax.setCellValueFactory(new PropertyValueFactory("taxa_cambio"));
    }
}
