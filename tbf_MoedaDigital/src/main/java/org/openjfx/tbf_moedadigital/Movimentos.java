/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author lucas
 */
public class Movimentos {
    private Integer id_movimento;
    private Integer id_destino_movimento;
    private Integer id_origem_movimento;
    private String iban_movimento;
    private Integer referencia_movimento;
    private Integer entidade_movimento;
    private Double montante_movimento;
    private Integer tipo_movimento;
    private Date registo_movimento;

    public Movimentos(){
    
    }

    public Movimentos(Integer id_movimento, Integer id_destino_movimento, Integer id_origem_movimento, String iban_movimento, Integer referencia_movimento, Integer entidade_movimento, Double montante_movimento, Integer tipo_movimento, Date registo_movimento) {
        this.id_movimento = id_movimento;
        this.id_destino_movimento = id_destino_movimento;
        this.id_origem_movimento = id_origem_movimento;
        this.iban_movimento = iban_movimento;
        this.referencia_movimento = referencia_movimento;
        this.entidade_movimento = entidade_movimento;
        this.montante_movimento = montante_movimento;
        this.tipo_movimento = tipo_movimento;
        this.registo_movimento = registo_movimento;
    }
    
    public Movimentos (Integer entidade_movimento, Integer referencia_movimento, Double montante_movimento){
        this.referencia_movimento = referencia_movimento;
        this.entidade_movimento = entidade_movimento;
        this.montante_movimento = montante_movimento;
    }
    
    public Movimentos (double valor, String iban){
        this.montante_movimento = valor;
        this.iban_movimento = iban;
    }
    
    public Integer getId_movimento() {
        return id_movimento;
    }

    public void setId_movimento(Integer id_movimento) {
        this.id_movimento = id_movimento;
    }

    public Integer getId_destino_movimento() {
        return id_destino_movimento;
    }

    public void setId_destino_movimento(Integer id_destino_movimento) {
        this.id_destino_movimento = id_destino_movimento;
    }

    public Integer getId_origem_movimento() {
        return id_origem_movimento;
    }

    public void setId_origem_movimento(Integer id_origem_movimento) {
        this.id_origem_movimento = id_origem_movimento;
    }

    public String getIban_movimento() {
        return iban_movimento;
    }

    public void setIban_movimento(String iban_movimento) {
        this.iban_movimento = iban_movimento;
    }

    public Integer getReferencia_movimento() {
        return referencia_movimento;
    }

    public void setReferencia_movimento(Integer referencia_movimento) {
        this.referencia_movimento = referencia_movimento;
    }

    public Integer getEntidade_movimento() {
        return entidade_movimento;
    }

    public void setEntidade_movimento(Integer entidade_movimento) {
        this.entidade_movimento = entidade_movimento;
    }

    public Double getMontante_movimento() {
        return montante_movimento;
    }

    public void setMontante_movimento(Double montante_movimento) {
        this.montante_movimento = montante_movimento;
    }

    public Integer getTipo_movimento() {
        return tipo_movimento;
    }

    public void setTipo_movimento(Integer tipo_movimento) {
        this.tipo_movimento = tipo_movimento;
    }

    public Date getRegisto_movimento() {
        return registo_movimento;
    }

    public void setRegisto_movimento(Date registo_movimento) {
        this.registo_movimento = registo_movimento;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id_movimento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movimentos other = (Movimentos) obj;
        if (!Objects.equals(this.id_movimento, other.id_movimento)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Movimentos{" + "id_movimento=" + id_movimento + ", id_destino_movimento=" + id_destino_movimento + ", id_origem_movimento=" + id_origem_movimento + ", iban_movimento=" + iban_movimento + ", referencia_movimento=" + referencia_movimento + ", entidade_movimento=" + entidade_movimento + ", montante_movimento=" + montante_movimento + ", tipo_movimento=" + tipo_movimento + ", registo_movimento=" + registo_movimento + '}';
    }
     
}