package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;



/**
 *
 * @author Telmo Santos (1181838)
 */
public class Helper {

    public static final ConnDB con = new ConnDB();
    private static Utilizadores LoginUser;
    public static Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

    public static Utilizadores getLoginUser() {
        return LoginUser;
    }

    public static void setLoginUser(Utilizadores LoginUser) {
        Helper.LoginUser = LoginUser;
    }

    /**
     * Mensagem de alerta
     *
     * @param message
     */
    public static void showInfoAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Info");
        alert.setContentText(message);
        alert.showAndWait();
    }
    /**
     * Mensage de erro
     *
     * @param message
     */
    public static void showErrorAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Error");
        alert.setContentText(message);
        alert.showAndWait();
    }
    /**
     * Mensage de alerta quando é criado um novo user
     *
     * @param message
     */
    public static void showAlertGravar(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Cliente Gravado");
        alert.setContentText(message);
        alert.showAndWait();
    }
    
     public static boolean validarPassword(String strPassword) {
        String regexValidator = "^(?=.*?[A-Z])(?=.*?[0-9])(?=.*\\W).{6,}$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strPassword);
        boolean result = m.find();
        return result;
    }
     
    public static boolean validarNumContrato(String strnumContrato) {
        String regexValidator = "^([a-zA-Z0-9]+){3,32}$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strnumContrato);
        boolean result = m.find();
        return result;
    }
     public static boolean validarUpdateNumContrato(String strnumContrato) {
        String regexValidator = "^([a-zA-Z0-9]+){0,}$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strnumContrato);
        boolean result = m.find();
        return result;
    }
      /**
     * Valida o nome do utilizador Aceita nomes"
     *
     * @param strUsername
     * @return
     */
    public static boolean validarNome(String strUsername) {
        String regexValidator = "(?=^.{2,255}$)^[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+(?:[ ](?:das?|dos?|de|e|[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+))*$";
        Pattern r = Pattern.compile(regexValidator);
        Matcher m = r.matcher(strUsername);
        boolean result = m.find();
        return result;
    }
        /**
     * Valida o NIF
     *
     * @param strNIF
     * @return
     */
    public static boolean validarNIF(String strNIF) {
        boolean result = true;
        if (!validarInteger(strNIF)) {
            result = false;
        }
        if (strNIF.length() > 9) {
            result= false;
        }
        return result;
    }

    /**
     * Valida qualquer numero inteiro com base no texto enviado por parametro
     *
     * @param strNum
     * @return boolean
     */
    public static boolean validarInteger(String strNum) {
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    public static void InsertUpdateDeleteBD(String cmd) throws SQLException, ClassNotFoundException{
        
        Connection conn = ConnDB.getConnDB();
        
        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        preparedStatement.executeUpdate(cmd);
        
        preparedStatement.close();
    }
    public static  void AlertConfirmar(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Cliente Gravado");
        alert.setContentText(message);
        alert.showAndWait();

    }
public static  void AlertVazio(String message){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();

    }
}


