/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;


import java.io.IOException;
import java.net.URL;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.openjfx.tbf_moedadigital.Utilizadores.UtilizadoresLista;


/**
 * FXML Controller class
 *
 * @author bruno
 */
public class GestaoClientesController implements Initializable {


    @FXML
    private AnchorPane anchor;
    @FXML
    private TableView<UtilizadoresLista> tabListagemUtilizadores;
    @FXML
    private TableColumn<UtilizadoresLista,String> tabID;
    @FXML
    private TableColumn<UtilizadoresLista,String> tabNome;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabNif;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabMorada;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabEmail;
    @FXML
    private TableColumn<UtilizadoresLista, String> tabCodCliente;
    @FXML
    private TextField txtFMorada;
    @FXML
    private TextField txtFEmail;
    
    //atributos
    private String morada_utilizador;
    private String email_utilizador;


    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try {
            bindGrid();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(GestaoClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    private void bindGrid() throws SQLException, ClassNotFoundException {
       
        ArrayList<UtilizadoresLista> lst = UtilizadoresLista.getListaUtilizadores();
        ObservableList<UtilizadoresLista> obslst = FXCollections.observableArrayList(lst);
        tabListagemUtilizadores.setItems(obslst);
        tabID.setCellValueFactory(new PropertyValueFactory("id_utilizador"));
        tabNome.setCellValueFactory(new PropertyValueFactory("nome_utilizador"));
        tabNif.setCellValueFactory(new PropertyValueFactory("nif_utilizador"));
        tabMorada.setCellValueFactory(new PropertyValueFactory("morada_utilizador"));
        tabEmail.setCellValueFactory(new PropertyValueFactory("email_utilizador"));
        tabCodCliente.setCellValueFactory(new PropertyValueFactory("utilizador_num_contrato"));
    
    }

    @FXML
    private void bttCriar_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException { //Novo Cliente
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("NovoCliente.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setResizable(false);
            stage.setTitle("TBF Client Management");
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
            bindGrid();
        } catch(IOException | ClassNotFoundException | SQLException e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        } finally {
            bindGrid();
        }
    }

    @FXML
    private void bttEditar_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException { //Editar Cliente
        
            if (tabListagemUtilizadores.getSelectionModel().getSelectedItem() == null) {
                Helper.showErrorAlert("Necessário selecionar um cliente que queira editar!");
                return;
            }
            editar();
    }

    @FXML
    private void bttEliminar_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException { //Eliminar Cliente
        
        if (tabListagemUtilizadores.getSelectionModel().getSelectedItem() == null) {
            Helper.showErrorAlert("Necessário selecionar um cliente!");
            return;
        }
        
        UtilizadoresLista selected = tabListagemUtilizadores.getSelectionModel().getSelectedItem();
        int id = selected.getId_utilizador();
        
        String cmd = "DELETE FROM `LAPR2_G5`.`Utilizador` WHERE (`id_utilizador` = '"+id+"');";
        
 
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("TBF Client Management");
        alert.setHeaderText("AVISO: Vai eliminar este cliente!");
        alert.setContentText("Deseja mesmo Continuar?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");
        
        alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();        
        
        if (result.get() == buttonTypeYES) { 
            Helper.InsertUpdateDeleteBD(cmd);
            bindGrid();
        } else {
            event.consume();
            bindGrid();
        }

    }

    @FXML
    private void btt_recarregar(ActionEvent event) throws SQLException, ClassNotFoundException {
        bindGrid();
    }
    
    @FXML
    private void bttnGravar(ActionEvent event) throws ClassNotFoundException, SQLException {
        gravarEdit();
    }
    
    private void editar(){
        
        UtilizadoresLista selected = tabListagemUtilizadores.getSelectionModel().getSelectedItem();
        txtFMorada.setText(selected.getMorada_utilizador());
        txtFEmail.setText(selected.getEmail_utilizador());

    }
    
    private void gravarEdit() throws SQLException, ClassNotFoundException{
        
        UtilizadoresLista selected = tabListagemUtilizadores.getSelectionModel().getSelectedItem();
        
        morada_utilizador = txtFMorada.getText();
        email_utilizador = txtFEmail.getText();
        int id = selected.getId_utilizador();
        
        String cmd = "UPDATE `LAPR2_G5`.`Utilizador` SET `email_utilizador` = '"+email_utilizador+"', `morada_utilizador` = '"+morada_utilizador+"' "
                + "WHERE (`id_utilizador` = '"+id+"')";
        Helper.InsertUpdateDeleteBD(cmd);
        Helper.showAlertGravar("Cliente editado e gravado com sucesso");
        bindGrid();
    }
}