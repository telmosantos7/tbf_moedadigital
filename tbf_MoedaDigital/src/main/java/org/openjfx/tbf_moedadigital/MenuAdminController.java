/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

 

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.event.EventHandler;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class MenuAdminController implements Initializable {

 


    @FXML
    private Button btLogOff;
    @FXML
    private Button btn_GestaoClientes;
    @FXML
    private Button btn_GestaoMoeda;
    @FXML
    private Button btn_SimularCredito;
    
    @FXML
    private Button btn_GestaoMovimentos;
    
    @FXML
    private AnchorPane userPNG;
    @FXML
    private Button btn_MensagensInternas;
    @FXML
    private AnchorPane MensagensInternasPane;
    @FXML
    private Button btn_ClinetesRegistados;
    @FXML
    private Button btn_ClientesHabitacao;
    @FXML
    private Button btn_ClientesPessoal;
    @FXML
    private Button btn_ClientesAuto;
    @FXML
    private Button btn_ClientesSemCredito;
    @FXML
    private Label lblUser123;
    @FXML
    private Button estatisticaID;
    @FXML
    private Button agendaID;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            MensagensInternasPane.setVisible(false);
            lblUser123.setText(Helper.getLoginUser().getNome_utilizador());
    }

 

    @FXML
    private void btn_GestaoClientes_OnAction(ActionEvent event) throws IOException {
        MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GestaoClientes.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }

 

    @FXML
    private void btn_GestaoMoeda_OnAction(ActionEvent event) {
        MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GestaoMoeda.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Currencies Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }

 

    @FXML
    private void btn_SimularCredito_OnAction(ActionEvent event) {
        MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SimuladorCredito.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    }
        
    @FXML
    private void btLogOff_OnAction(ActionEvent event) {
        MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Login.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
            fechar();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
        finally{ fechar(); }
    }
    
     @FXML
    private void btGestaoMovimento_OnAction(ActionEvent event) {        
        MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SaldoMovimentoAdmin.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Admin Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
           
    }
    public void fechar(){
        Window window = userPNG.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    @FXML
    private void btn_MensagensInternas_OnAction(ActionEvent event) {
        MensagensInternasPane.setVisible(true);
            
    }

    @FXML
    private void btn_ClinetesRegistados_OnAction(ActionEvent event) throws SQLException, ClassNotFoundException, Exception {
        String cmd= "SELECT * FROM LAPR2_G5.Utilizador WHERE NOT tipo_utilizador = 1";
        String message = "Bem-Vindo ";
        SelectClientes(cmd, message);
    }

    @FXML
    private void btn_ClientesHabitacao_OnAction(ActionEvent event) throws ClassNotFoundException, Exception {
        String code = "SELECT id_utilizador_prestacao AS id_utilizador_prestacao FROM LAPR2_G5.Prestacoes WHERE id_credito_prestacao = 2 ";
        String message = "Crédito Habitação Características";
        String text = ""
            + "-> Finalidade\n" +
            "\n" +
            "	Compra, construção, ou obras de beneficiação, recuperação ou ampliação de  habitação permanente ou secundária e\n" +
            "	aquisição de garagem, desde que faça parte do mesmo bloco habitacional.\n" +
            "\n" +
            "-> Reembolso\n" +
            "\n" +
            "	Prestações constantes, de capital e juros.\n" +
            "\n" +
            "-> Montante\n" +
            "\n" +
            "	Mínimo de 5.000 euros.\n" +
            "\n" +
            "	Montante máximo:\n" +
            "	- Habitação Própria Permanente: o menor valor entre 80 a 85% do valor da avaliação (dependente da análise da proposta) e \n" +
            "		90% do valor de investimento (Aquisição, Construção ou Obras);\n" +
            "	- Habitação Própria Secundária: o menor valor entre 80% do valor de avaliação e \n" +
            "		80% do valor de investimento (Aquisição, Construção ou Obras).\n" +
            "\n" +
            "-> Prazo\n" +
            "\n" +
            "	O prazo máximo do empréstimo é de 40 anos desde que a idade máxima dos proponentes, no termo do empréstimo, não exceda os 80 anos.\n" +
            "\n" +
            "-> Garantia\n" +
            "	\n" +
            "	Para garantia do empréstimo deverá ser constituída hipoteca específica da habitação a financiar.\n" +
            "\n" +
            "-> Taxa de Juro\n" +
            "	Taxa variável indexada à Euribor:\n" +
            "	Taxa de base variável indexada à Euribor a 12 meses, acrescida de spread.\n" +
            "\n" +
            "	Taxa mista (Fixa + Variável):\n" +
            "	Período inicial: Taxa fixa a 5, 7, 10, 15, 20, 25 ou 30 anos ou Taxa de base fixa, de 5 a 25 anos, acrescida de spread.\n" +
            "	Prazo remanescente: Taxa variável indexada à Euribor a 12 meses, acrescida de spread.\n" +
            "\n" +
            "	Taxa Fixa(1):\n" +
            "	Taxa fixa a 5, 7, 10, 15, 20, 25 ou 30 anos ou Taxa de base fixa, de 5 a 25 anos, acrescida de spread, aplicável durante todo o prazo do contrato.\n" +
            "\n" +
            "\n" +
            "Para mais informações diriga-se a um balcão TBF perto de si ou fale com nosco através do nosso email.\n" +
            "\n" +
            "Contactos,\n" +
            " 	\n" +
            "tbfmoedadigital@gmail.com\n" +
            "	";
        UtilizadoresSendMailByTipoCredito(code, message, text);
    }

    @FXML
    private void btn_ClientesPessoal_OnAction(ActionEvent event) throws ClassNotFoundException, Exception {
        String code = "SELECT id_utilizador_prestacao AS id_utilizador_prestacao FROM LAPR2_G5.Prestacoes WHERE id_credito_prestacao = 1 ";
        String message = "Crédio Pessoal Características";
        String text = ""
                + "-> Finalidade\n" +
            "\n" +
            "	Financia as despesas com bens ou serviços de consumo geral, até 5.000 euros e com prazos até 48 meses, \n" +
            "	para que possa satisfazer as suas necessidades pessoais ou familiares associadas a pequenos projetos.\n" +
            "\n" +
            "	* Estão excluídas as despesas com a saúde, formação e energias renováveis.\n" +
            "\n" +
            "-> Reembolso\n" +
            "\n" +
            "	Prestações mensais, constantes (de capital e juros).\n" +
            "\n" +
            "-> Montantes\n" +
            "\n" +
            "	Mínimo: 2.000€\n" +
            "	Máximo: 5.000€\n" +
            "\n" +
            "-> Prazos\n" +
            "\n" +
            "	Mínimo: 6 meses\n" +
            "	Máximo: 36 meses (3 anos)\n" +
            "\n" +
            "-> Seguros e Garantias\n" +
            "	\n" +
            "	Não aplicáveis.\n" +
            "\n" +
            "Para mais informações diriga-se a um balcão TBF perto de si ou fale com nosco através do nosso email.\n" +
            "\n" +
            "Contactos,\n" +
            " 	\n" +
            "tbfmoedadigital@gmail.com";
        UtilizadoresSendMailByTipoCredito(code, message, text);
    }

    @FXML
    private void btn_ClientesAuto_OnAction(ActionEvent event) throws ClassNotFoundException, Exception {
        String code = "SELECT id_utilizador_prestacao AS id_utilizador_prestacao FROM LAPR2_G5.Prestacoes WHERE id_credito_prestacao = 3 ";
        String message = "Crédito Automóvel Características";
        String text = "  \n" +
            "-> Finalidade\n" +
            "\n" +
            "	Financiamento da aquisição de viaturas novas ou usadas, ligeiras de passageiros ou mistas, sem reserva de propriedade.\n" +
            "\n" +
            "-> Reembolso\n" +
            "\n" +
            "	Prestações mensais, constantes (de capital e juros)\n" +
            "\n" +
            "-> Montante\n" +
            "\n" +
            "	Mínimo: 5.000 €\n" +
            "	Máximo: 75.000€ (inclusive)\n" +
            "\n" +
            "-> Prazos\n" +
            "	\n" +
            "	Mínimo: 12 meses\n" +
            "	Máximo: 120 meses.\n" +
            "\n" +
            "-> Garantia\n" +
            "	\n" +
            "	PPara garantia do seu empréstimo pode constituir fiança.\n" +
            "\n" +
            "-> Taxa de Juro*\n" +
            "	\n" +
            "	A taxa de juro pode ser indexada à Euribor 12M ou taxa base fixa, entre 2 e 10 anos, acrescida de spread.\n" +
            "\n" +
            "-> Outras condições\n" +
            "\n" +
            "	Registo de propriedade de imediato em nome do Cliente.\n" +
            "\n" +
            "	Seguro obrigatório de responsabilidade civil automóvel.\n" +
            "\n" +
            "Para mais informações diriga-se a um balcão TBF perto de si ou fale com nosco através do nosso email.\n" +
            "\n" +
            "Contactos,\n" +
            " 	\n" +
            "tbfmoedadigital@gmail.com\n" +
            "	";
        UtilizadoresSendMailByTipoCredito(code, message, text);
    }

    @FXML
    private void btn_ClientesSemCredito_OnAction(ActionEvent event) throws ClassNotFoundException, Exception{
        String code = "SELECT id_utilizador_prestacao AS id_utilizador_prestacao FROM LAPR2_G5.Prestacoes WHERE estado_prestacao = 1";
        String message = "TBF - Soluções para si";
        String text = " , como vai?\n" +
            "\n" +
            "Depois de algum tempo mantendo excelente relacionamento consigo, sabemos do seu apreço por melhorar o seu bem estar e qualidade de vida.\n" +
            "\n" +
            "Por isso, gostaríamos de apresentar algumas das novas soluções que desenvolvemos.\n" +
            "\n" +
            "Diriga-se as nossas instalações para obter informações sobre as nossas soluções para si e para a sua familía!\n" +
            "\n" +
            "Abraço,\n" +
            "\n" +
            "Gerente TBF\n"
            +""+Helper.getLoginUser().getNome_utilizador();
        sendEmailClientesSemCredito(code , message, text);
    }
    @FXML
    private void EstatisticaOnAction (ActionEvent event) throws ClassNotFoundException, Exception{
     MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Estatisticas.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    
    }
      @FXML
    private void AgendaOnAction (ActionEvent event) throws ClassNotFoundException, Exception{
     MensagensInternasPane.setVisible(false);
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Agenda.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root)); 
            stage.setTitle("TBF Client Management");
            stage.setResizable(false);
            Image anotherIcon = new Image("file:TBF.png");
            stage.getIcons().add(anotherIcon);
            stage.show();
        } catch(Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        }
    
    }
 
 
    
    
    public void SelectClientes(String cmd, String message) throws SQLException, ClassNotFoundException, Exception{
        Connection conn = ConnDB.getConnDB();
        Utilizadores item = new Utilizadores();
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            ResultSet rs = preparedStatement.executeQuery(cmd);
            while (rs.next()) { 
                item.setNome_utilizador(rs.getString("nome_utilizador"));
                item.setEmail_utilizador(rs.getString("email_utilizador"));
                sendEmail.sendMail(item.getEmail_utilizador(), message+ item.getNome_utilizador(),"Seja bem-vindo ao seu banco preferêncial TBF!");
            }
        preparedStatement.close();
        }
    }
    public ArrayList<Utilizadores> SelectClientesByCredito(String cmd) throws SQLException, ClassNotFoundException, Exception{
        ArrayList<Utilizadores> lst = new ArrayList();
        Connection conn = ConnDB.getConnDB();
        Utilizadores item = new Utilizadores();
        
        try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
            ResultSet rs = preparedStatement.executeQuery(cmd);
            while (rs.next()) { 
                item.setId_utilizador(rs.getInt("id_utilizador_prestacao"));
                
                lst.add(item);
            }
        preparedStatement.close();
        }
    return lst;
    }
    
    public void UtilizadoresSendMailByTipoCredito(String code, String message, String text) throws SQLException, ClassNotFoundException, Exception{
        Connection conn = ConnDB.getConnDB();

        ArrayList<Utilizadores> array = SelectClientesByCredito(code);
        
        for(Utilizadores item : array){
            String cmd = "Select * FROM LAPR2_G5.Utilizador WHERE id_utilizador = "+ item.getId_utilizador();
        
            try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
                ResultSet rs = preparedStatement.executeQuery(cmd);
                while (rs.next()) {

                    item.setId_utilizador(rs.getInt("id_utilizador"));
                    item.setNome_utilizador(rs.getString("nome_utilizador"));
                    item.setEmail_utilizador(rs.getString("email_utilizador"));  
                    sendEmail.sendMail(item.getEmail_utilizador(), message, "Olá, " + item.getNome_utilizador() +" estas são as caracteristicas do seu crédito\n"+ text);
                }
            preparedStatement.close();
            Helper.showInfoAlert("Mensagens enviadas com sucesso!");
            }
        }
    }
    
        public void sendEmailClientesSemCredito(String code, String message, String text) throws SQLException, ClassNotFoundException, Exception{
        Connection conn = ConnDB.getConnDB();

        ArrayList<Utilizadores> array = SelectClientesByCredito(code);
        
        for(Utilizadores item : array){
            String cmd = "SELECT * FROM LAPR2_G5.Utilizador WHERE NOT id_utilizador = "+ item.getId_utilizador() +" and NOT tipo_utilizador = 1";
        
            try (PreparedStatement preparedStatement = conn.prepareStatement(cmd)) {
                ResultSet rs = preparedStatement.executeQuery(cmd);
                while (rs.next()) {

                    item.setId_utilizador(rs.getInt("id_utilizador"));
                    item.setNome_utilizador(rs.getString("nome_utilizador"));
                    item.setEmail_utilizador(rs.getString("email_utilizador"));  
                    sendEmail.sendMail(item.getEmail_utilizador(), message, "Olá, " + item.getNome_utilizador() +""+ text);
                }
            preparedStatement.close();
            Helper.showInfoAlert("Mensagens enviadas com sucesso!");
            }
        }
    }

}