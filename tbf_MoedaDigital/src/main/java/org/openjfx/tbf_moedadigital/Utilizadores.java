/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.scene.chart.PieChart.Data;

/**
 *
 * @author Pc
 */
public class Utilizadores {
    
    private int id_utilizador = 0; 
    private String utilizador_num_contrato;
    private String password_utilizador; 
    private String nome_utilizador; 
    private String morada_utilizador;
    private String email_utilizador;
    private int utilizador_id_moeda;
    private double saldo_utilizador;
    private String nif_utilizador;
    private Data registo_utilizador;
    private int tipo_utilizador;
    private int selecionado = 0;
    private String iban_utilizador;

    public static final String SQL_SELECT = "SELECT id_utilizador,utilizador_num_contrato,password_utilizador,nome_utilizador,morada_utilizador,email_utilizador,utilizador_id_moeda,"
            + "saldo_utilizador,tipo_utilizador,registo_utilizador,nif_utilizador FROM Utilizador ";
    
    public static final String SQL_SELECT_UTILIZADOR = "SELECT id_utilizador,utilizador_num_contrato,nome_utilizador,morada_utilizador,email_utilizador,nif_utilizador FROM Utilizador ";

    public int getId_utilizador() {
        return id_utilizador;
    }

    public void setId_utilizador(int id_utilizador) {
        this.id_utilizador = id_utilizador;
    }

    public String getUtilizador_num_contrato() {
        return utilizador_num_contrato;
    }

    public void setUtilizador_num_contrato(String utilizador_num_contrato) {
        this.utilizador_num_contrato = utilizador_num_contrato;
    }

    public  String getPassword_utilizador() {
        return password_utilizador;
    }

    public void setPassword_utilizador(String password_utilizador) {
        this.password_utilizador = password_utilizador;
    }

    public String getNome_utilizador() {
        return nome_utilizador;
    }

    public void setNome_utilizador(String nome_utilizador) {
        this.nome_utilizador = nome_utilizador;
    }

    public String getMorada_utilizador() {
        return morada_utilizador;
    }

    public void setMorada_utilizador(String morada_utilizador) {
        this.morada_utilizador = morada_utilizador;
    }

    public String getEmail_utilizador() {
        return email_utilizador;
    }

    public void setEmail_utilizador(String email_utilizador) {
        this.email_utilizador = email_utilizador;
    }

    public int getUtilizador_id_moeda() {
        return utilizador_id_moeda;
    }

    public void setUtilizador_id_moeda(int utilizador_id_moeda) {
        this.utilizador_id_moeda = utilizador_id_moeda;
    }

    public double getSaldo_utilizador() {
        return saldo_utilizador;
    }

    public void setSaldo_utilizador(double saldo_utilizador) {
        this.saldo_utilizador = saldo_utilizador;
    }

    public Data getRegisto_utilizador() {
        return registo_utilizador;
    }

    public void setRegisto_utilizador(Data registo_utilizador) {
        this.registo_utilizador = registo_utilizador;
    }

    public int getTipo_utilizador() {
        return tipo_utilizador;
    }

    public void setTipo_utilizador(char tipo_utilizador) {
        this.tipo_utilizador = tipo_utilizador;
    }
    
    public String getNif_utilizador() {
        return nif_utilizador;
    }

    public void setNif_utilizador(String nif_utilizador) {
        this.nif_utilizador = nif_utilizador;
    }
       public Utilizadores(){
        this.id_utilizador = 0;
    }

    public String getIban_utilizador() {
        return iban_utilizador;
    }

    public void setIban_utilizador(String iban_utilizador) {
        this.iban_utilizador = iban_utilizador;
    }
    
    public Utilizadores(int id_utilizador,String utilizador_num_contrato, String password_utilizador, String iban_utilizador, String nome_utilizador){
        this.utilizador_num_contrato= utilizador_num_contrato;
        this.id_utilizador= id_utilizador;
        this.password_utilizador= password_utilizador;
        this.iban_utilizador= iban_utilizador;
        this.nome_utilizador = nome_utilizador;
    
    }
    public Utilizadores(String utilizador_num_contrato, String email_utilizador, int id_utilizador, String password_utilizador, String nome_utilizador ){
        this.utilizador_num_contrato= utilizador_num_contrato;
        this.id_utilizador= id_utilizador;
        this.email_utilizador= email_utilizador;
        this.password_utilizador= password_utilizador;
        this.nome_utilizador = nome_utilizador;
    }
    
    public int getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(int selecionado) {
        this.selecionado = selecionado;
    }
    
    public Utilizadores(String utilizador_num_contrato, String email_utilizador, String nome_utilizador){
        this.utilizador_num_contrato= utilizador_num_contrato;
        this.email_utilizador= email_utilizador;
        this.nome_utilizador = nome_utilizador;
    
    }
    
    public Utilizadores(int id_utilizador, String nome_utilizador,String nif_utilizador, String morada_utilizador, String email_utilizador, String utilizador_num_contrato, int selecionado){
        this.utilizador_num_contrato = utilizador_num_contrato;
        this.id_utilizador = id_utilizador;
        this.nome_utilizador = nome_utilizador;
        this.nif_utilizador = nif_utilizador;
        this.email_utilizador = email_utilizador;
        this.morada_utilizador = morada_utilizador;
        this.selecionado = selecionado;
    }

    public static class UtilizadoresLista {

        private int id_utilizador;
        private String nome_utilizador;
        private String nif_utilizador;
        private String morada_utilizador;
        private String email_utilizador;
        private String utilizador_num_contrato;

        public int getId_utilizador() {
            return id_utilizador;
        }

        public void setId_utilizador(int id_utilizador) {
            this.id_utilizador = id_utilizador;
        }
        
        public String getNome_utilizador() {
            return nome_utilizador;
        }

        public void setNome_utilizador(String nome_utilizador) {
            this.nome_utilizador = nome_utilizador;
        }

        public String getNif_utilizador() {
            return nif_utilizador;
        }

        public void setNif_utilizador(String nif_utilizador) {
            this.nif_utilizador = nif_utilizador;
        }

        public String getMorada_utilizador() {
            return morada_utilizador;
        }

        public void setMorada_utilizador(String morada_utilizador) {
            this.morada_utilizador = morada_utilizador;
        }

        public String getEmail_utilizador() {
            return email_utilizador;
        }

        public void setEmail_utilizador(String email_utilizador) {
            this.email_utilizador = email_utilizador;
        }

        public String getUtilizador_num_contrato() {
            return utilizador_num_contrato;
        }

        public void setUtilizador_num_contrato(String utilizador_num_contrato) {
            this.utilizador_num_contrato = utilizador_num_contrato;
        }
    
    
        public static ArrayList<UtilizadoresLista> getListaUtilizadores() throws SQLException, ClassNotFoundException {
            ArrayList<UtilizadoresLista> lst = new ArrayList();
            Connection conn = ConnDB.getConnDB();
            String cmd ;
            
            try {
                cmd = "Select id_utilizador, nome_utilizador, morada_utilizador, email_utilizador, nif_utilizador, utilizador_num_contrato, tipo_utilizador FROM LAPR2_G5.Utilizador WHERE tipo_utilizador = '2'";
                PreparedStatement preparedStatement = conn.prepareStatement(cmd);
                
                ResultSet rs = preparedStatement.executeQuery(cmd);
                
                while (rs.next()) {
                    
                    UtilizadoresLista item = new UtilizadoresLista();
                    item.setId_utilizador(rs.getInt("id_utilizador"));
                    item.setNome_utilizador(rs.getString("nome_utilizador"));
                    item.setNif_utilizador(rs.getString("nif_utilizador"));
                    item.setMorada_utilizador(rs.getString("morada_utilizador"));
                    item.setEmail_utilizador(rs.getString("email_utilizador"));
                    item.setUtilizador_num_contrato(rs.getString("utilizador_num_contrato"));
                    
                    lst.add(item);
                }
                preparedStatement.close();
            }catch(Exception e){
                System.err.println("ERRO: " + e.getMessage());
            }

            return lst;
        }
    
    } 
    public void RegistarCliente(Utilizadores newutilizador) throws SQLException, ClassNotFoundException{

    }
    
    @Override
    public String toString() {
        return "Utilizadores{" + "id_utilizador=" + id_utilizador + ", saldo_utilizador=" + saldo_utilizador + '}';
    }
    
    
    }

