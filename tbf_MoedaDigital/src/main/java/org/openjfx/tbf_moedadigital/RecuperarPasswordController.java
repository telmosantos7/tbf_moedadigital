/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
/**
 * FXML Controller class
 *
 * @author bruno
 */
public class RecuperarPasswordController implements Initializable {


    @FXML
    private TextField txtFUserRecuperar;
    @FXML
    private TextField txtFEmailRecuperar;
    @FXML
    private AnchorPane recuperarPass;
    private static final Random RANDOM = new SecureRandom();
    private static final int PASSWORD_LENGTH = 10;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void bttnRecuperar_OnAction(ActionEvent event) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, Exception {
       
        switch (validarDadosRecuperar()) {
            case 1:
                Helper.showInfoAlert("A sua nova password foi enviada para o seu email!");
                break;
            case 2:
                Helper.showErrorAlert("Nome de Utilizador ou email inválido(a)!");
                break;
            default:
                Helper.showErrorAlert("Serviço indiponivél\nTente novamente mais tarde");
                break;
        }
        fechar();
    }
    
    public int validarDadosRecuperar() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, Exception {
        
        int validar = 0;
        String utilizador_num_contrato = txtFUserRecuperar.getText();
        String email_utilizador = txtFEmailRecuperar.getText();
        
        Utilizadores user = getUtilizadorByIdRecuperar(utilizador_num_contrato);
        
        if (user.getId_utilizador() != 0 && user.getEmail_utilizador().equals(email_utilizador)) {

            validar = 1;
            String emailUser = getUtilizadorByIdRecuperar(utilizador_num_contrato).getEmail_utilizador();
            String password = getUtilizadorByIdRecuperar(utilizador_num_contrato).getPassword_utilizador();
            sendEmail.sendMail(emailUser,"Nova Password", "Nova password: " + password);
        } else {
            validar =2;
        }
        
    return validar;
    }
    
    public static Utilizadores getUtilizadorByIdRecuperar(String user) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        
        Connection conn = ConnDB.getConnDB();
        String cmd;
        Utilizadores obj = new Utilizadores();
        Utilizadores uu = new Utilizadores();
       try {
            cmd = "SELECT id_utilizador,email_utilizador,codigo_acesso_utilizador, nome_utilizador FROM LAPR2_G5.Utilizador WHERE utilizador_num_contrato='" + user + "'";

            PreparedStatement statement = conn.prepareStatement(cmd);
            ResultSet rs = statement.executeQuery(cmd);
 
            while (rs.next()) {
                obj.setId_utilizador(rs.getInt("id_utilizador"));
                obj.setUtilizador_num_contrato(user);
                obj.setEmail_utilizador(rs.getString("email_utilizador"));
                obj.setPassword_utilizador(rs.getString("codigo_acesso_utilizador"));
                obj.setNome_utilizador(rs.getString("nome_utilizador"));
            }
           statement.close();
          
           uu = new Utilizadores (obj.getUtilizador_num_contrato(), obj.getEmail_utilizador(),obj.getId_utilizador(),obj.getPassword_utilizador(),obj.getNome_utilizador());

       } catch (SQLException ex) {
          System.err.println("Erro: " + ex.getMessage());
        }
       return uu;
   }
    public void fechar(){
        Window window = recuperarPass.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}
