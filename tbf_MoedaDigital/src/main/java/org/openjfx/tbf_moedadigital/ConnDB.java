/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 *
 * @author Pc
 */
public class ConnDB {
    private static Connection conn;
    private static String db_url = "jdbc:mysql://193.136.62.207:3306/LAPR2_G5?useSSL=false";
    private static String db_user = "LAPR2_G5";
    private static String db_pass = "Tbf123";
 
    public static Connection getConnDB() throws SQLException, ClassNotFoundException{

        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException cnfe){
             System.err.println("Error:"+cnfe.getMessage());
        }
        conn = DriverManager.getConnection(db_url,db_user, db_pass);
        return conn;
    }
    public static Connection getConnection ()throws SQLException, ClassNotFoundException{
        if(conn !=null && conn.isClosed())
            return conn;
        getConnDB();
        return conn;
    }
    
    public List executeQuery(String queryString) throws ClassNotFoundException {
        List lst = null;

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = getConnection();
            try {
                stmt = con.createStatement();
                rs = stmt.executeQuery(queryString);
                lst = resultSetToList(rs);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return lst;
    }
    
    private List<Map<String, Object>> resultSetToList(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, Object>> rows = new ArrayList<>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            rows.add(row);
        }
        return rows;
    }
}
