/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author lucas
 */
public class PagamentosServicosController implements Initializable {
    
    @FXML
    private Button btnPagamento;
    
    @FXML
    private Button btnTransferencia;
    
    @FXML
    private Button btnVoltar;
    
    @FXML
    private Button btnDeposito;
    
    @FXML
    private Button btnConfirmarTransferencia;
    
    @FXML
    private Button btnConfirmarPagamento;
    
    @FXML
    private TextField txtIban;
    
    @FXML
    private TextField txtValorTransferencia;
    
    @FXML
    private TextField txtValorPagamento;
    
    @FXML
    private TextField txtEntidade;
    
    @FXML
    private TextField txtReferencia;
    
    @FXML
    private TextField txtSaldo;
    
    @FXML
    private Pane paneFormTransferencias;
    
    @FXML
    private Pane paneFormPagamentos;
    
    public void onBtnPagamentosAction() {
        if (paneFormTransferencias.isVisible() == true) {
            paneFormTransferencias.setVisible(false);
        }
        paneFormPagamentos.setVisible(true);
    }
    
    public void onBtnTransferenciasAction() {
        if (paneFormPagamentos.isVisible() == true) {
            paneFormPagamentos.setVisible(false);
            
        }
        paneFormTransferencias.setVisible(true);
    }
    
    public void onBtnDepositoAction() {
        try {
            Helper.showInfoAlert("Irá fazer um depóstio, deseja continuar?");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Deposito.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));            
            stage.setTitle("TBF Client Deposit");
            Image anotherIcon = new Image("file:TBF.png");
            stage.setResizable(false);
            stage.getIcons().add(anotherIcon);
            stage.show();
            fechar();
        } catch (Exception e) {
            String message = "Erro: " + e.getMessage();
            Helper.showErrorAlert(message);
        } finally {
            fechar();
        }
    }
    
    public void onBtnVoltarAction() {
        fechar();
    }
    
    public void onBtnConfirmarTransferenciasAction() {
        double valor = Double.parseDouble(txtValorTransferencia.getText());
        String Iban = txtIban.getText();
        System.out.println(valor + " " + Iban);
        double saldo = UtilizadorDAO.getSaldo();
        
        if (saldo < valor) {
            Helper.showErrorAlert("Saldo Insuficiente!");
        } else {
            MovimentosDao.insertMovimentoTrasnferencia(valor, Iban);
            Helper.showInfoAlert("Operação realizada com sucesso!");
            fechar();
        }
    }
    
    public void onBtnConfirmarPagamentosAction() {
        double valor = Double.parseDouble(txtValorPagamento.getText());
        int entidade = Integer.parseInt(txtEntidade.getText());
        int referencia = Integer.parseInt(txtReferencia.getText());
        
        double saldo = UtilizadorDAO.getSaldo();
        if (saldo < valor) {
            Helper.showErrorAlert("Saldo Insuficiente!");
        } else {
            MovimentosDao.insertMovimentoPagamento(entidade, referencia, valor);
            Helper.showInfoAlert("Operação realizada com sucesso!");
            fechar();
        }
    }
    
    public void fechar() {
        Stage stage = (Stage) btnVoltar.getScene().getWindow();
        stage.close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String saldo = String.valueOf(UtilizadorDAO.getSaldo());
        txtSaldo.setText(saldo);
        
    }
}
