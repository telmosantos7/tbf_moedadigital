/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Tiago
 */
public class PrestacoesDao {
    public static ArrayList<Prestacoes> getPrestacoes() throws ClassNotFoundException, SQLException  {
        ArrayList<Prestacoes> lst = new ArrayList();
         
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT id_utilizador_prestacao, id_credito_prestacao, registo_prestacao, total_prestacao FROM LAPR2_G5.Prestacoes "
                    + "WHERE data_inicio_prestacao < CURDATE() AND data_fim_prestacao > CURDATE()";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);

                while (rs.next()) {
                    
                    Prestacoes item = new Prestacoes();
                    item.setId_utilizador_prestacao(rs.getInt("id_utilizador_prestacao"));
                    item.setId_credito_prestacao(rs.getInt("id_credito_prestacao"));
                    item.setRegisto_prestacao(rs.getDate("registo_prestacao"));
                    item.setTotal_prestacao(rs.getDouble("total_prestacao"));

                    lst.add(item);
                }
                preparedStatement.close();
            conn.close();
            rs.close();
            return lst;
    }
        public static ArrayList<Prestacoes> getPrestacoesPorMes(String mes) throws ClassNotFoundException, SQLException  {
        ArrayList<Prestacoes> lst = new ArrayList();
        
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT id_utilizador_prestacao, id_credito_prestacao, registo_prestacao, total_prestacao FROM LAPR2_G5.Prestacoes WHERE data_inicio_prestacao < CURDATE() AND data_fim_prestacao > CURDATE()";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);

                while (rs.next()) {
                    
                    Prestacoes item = new Prestacoes();
                    item.setId_utilizador_prestacao(rs.getInt("id_utilizador_prestacao"));
                    item.setId_credito_prestacao(rs.getInt("id_credito_prestacao"));
                    item.setRegisto_prestacao(rs.getDate("registo_prestacao"));
                    item.setTotal_prestacao(rs.getDouble("total_prestacao"));

                    lst.add(item);
                }
                preparedStatement.close();
            conn.close();
            rs.close();
            return lst;
    }
}
