/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author lucas
 */
public class Credito {
    
    private Integer id_credito;
    private String descricao_credito;
    private String nome_credito;
    
    public Credito(){
    }

    public Credito(Integer id_credito, String descricao_credito, String nome_credito) {
        this.id_credito = id_credito;
        this.descricao_credito = descricao_credito;
        this.nome_credito = nome_credito;
    }

    public Integer getId_credito() {
        return id_credito;
    }

    public void setId_credito(Integer id_credito) {
        this.id_credito = id_credito;
    }

    public String getDescricao_credito() {
        return descricao_credito;
    }

    public void setDescricao_credito(String descricao_credito) {
        this.descricao_credito = descricao_credito;
    }

    public String getNome_credito() {
        return nome_credito;
    }

    public void setNome_credito(String nome_credito) {
        this.nome_credito = nome_credito;
    }

    @Override
    public String toString() {
        return this.descricao_credito;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id_credito);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Credito other = (Credito) obj;
        if (!Objects.equals(this.id_credito, other.id_credito)) {
            return false;
        }
        return true;
    }
   public static ArrayList<Credito> getCredito() throws ClassNotFoundException, SQLException  {
        ArrayList<Credito> lst = new ArrayList();
         
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT id_credito, descricao_credito, nome_credito FROM LAPR2_G5.Creditos ";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);

                while (rs.next()) {
                    
                    Credito item = new Credito();
                    item.setId_credito(rs.getInt("id_credito"));
                    item.setDescricao_credito(rs.getString("descricao_credito"));
                    item.setNome_credito(rs.getString("nome_credito"));

                    lst.add(item);
                }
                preparedStatement.close();
            
            return lst;
    }  
    
}
