package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import static org.openjfx.tbf_moedadigital.NovoClienteController.generateRandomCodigoAcesso;
import static org.openjfx.tbf_moedadigital.NovoClienteController.generateRandomUtilizadorNumContrato;

/**
 * FXML Controller class
 *
 * @author bruno
 */
public class EditarClienteController implements Initializable {

    @FXML
    private TextField txtFMorada;
    @FXML
    private TextField txtFEmail;
    @FXML
    private AnchorPane editarClienteScene;
    
    //atributos
    private String morada_utilizador;
    private String email_utilizador;
    private String cmd,cmd1;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        verificarInputText();
    } 


    @FXML
    private void bttnGravar(ActionEvent event) throws SQLException, ClassNotFoundException {
        

        Connection conn = ConnDB.getConnDB();
        
        Utilizadores item = new Utilizadores();
        int ss = item.getSelecionado();
        int id = 0;
        
        if( ss == 1){
            id =  item.getId_utilizador();
            item.setMorada_utilizador(txtFMorada.getText());
            item.setEmail_utilizador(txtFEmail.getText());
        }
        morada_utilizador = txtFMorada.getText();
        email_utilizador = txtFEmail.getText();


        cmd = "UPDATE LAPR2_G5.Utilizador SET email_utilizador = '"+email_utilizador+"', morada_utilizador = '"+morada_utilizador+"' WHERE id_utilizador = '" + id + "';";

        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        preparedStatement.executeUpdate(cmd);
        preparedStatement.close();
        Helper.showAlertGravar("Cliente/Admin editado com sucesso");

        fechar();
    }

    @FXML
    private void bttnSair(ActionEvent event) {
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("TBF Client Management");
        alert.setHeaderText("AVISO: Grave as alterações do cliente antes de sair!");
        alert.setContentText("Deseja mesmo sair?");
        
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");
        alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO); 
        
        Optional<ButtonType> result = alert.showAndWait();        
        if (result.get() == buttonTypeYES) {
            fechar();
        } else {
            event.consume();
        }
    }
    
        private void verificarInputText(){    
        txtFMorada.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFMorada.getText().length() > 50) {
                    txtFMorada.deleteNextChar();
                    Helper.showInfoAlert("O campo Morada não pode exceder 50 caracteres.");
                }
            }
        });
            txtFEmail.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> value, String oldValue, String newValue) {
                if (txtFEmail.getText().length() > 100) {
                    txtFEmail.deleteNextChar();
                    Helper.showInfoAlert("O campo Email não pode exceder 100 caracteres.");
                }
            }
        });
    }
    public void fechar(){
        Window window = editarClienteScene.getScene().getWindow();
        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}
