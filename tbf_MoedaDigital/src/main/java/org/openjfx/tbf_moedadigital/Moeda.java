/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.util.ArrayList;
import java.lang.String;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bruno
 */
public class Moeda {

    private int id_moeda;
    private String descricao_moeda;
    private String codigo_moeda;
    private double taxa_cambio;
    public final String SQL_SELECT = "SELECT id_Moeda, descricao_moeda, codigo_moeda FROM LAPR2_G5.Moeda ";

    public int getId_moeda() {
        return id_moeda;
    }

    public void setId_moeda(int id_moeda) {
        this.id_moeda = id_moeda;
    }

    public String getDescricao_moeda() {
        return descricao_moeda;
    }

    public void setDescricao_moeda(String descricao_moeda) {
        this.descricao_moeda = descricao_moeda;
    }

    public String getCodigo_moeda() {
        return codigo_moeda;
    }

    public void setCodigo_moeda(String codigo_moeda) {
        this.codigo_moeda = codigo_moeda;
    }

    public double getTaxa_cambio() {
        return taxa_cambio;
    }

    public void setTaxa_cambio(double taxa_cambio) {
        this.taxa_cambio = taxa_cambio;
    }
    public static ArrayList<Moeda> getLista() throws ClassNotFoundException, SQLException {
        return getListaMoedas("");
    }
    public static ArrayList<Moeda> getLista(String filter) throws ClassNotFoundException, SQLException {   
        return getListaMoedas(filter);
    }

    public static ArrayList<Moeda> getListaMoedas(String filter) throws ClassNotFoundException, SQLException  {
        ArrayList<Moeda> lst = new ArrayList();
         
            Connection conn = ConnDB.getConnDB();
            String cmd = "SELECT * FROM LAPR2_G5.Moeda ";
            
            PreparedStatement preparedStatement = conn.prepareStatement(cmd);    
            ResultSet rs = preparedStatement.executeQuery(cmd);

                while (rs.next()) {
                    
                    Moeda item = new Moeda();
                    item.setId_moeda(rs.getInt("id_moeda"));
                    item.setDescricao_moeda(rs.getString("descricao_moeda"));
                    item.setCodigo_moeda(rs.getString("codigo_moeda"));
                    item.setTaxa_cambio(rs.getDouble("taxa_cambio"));
                    lst.add(item);
                }
                preparedStatement.close();
            
            return lst;
    }
    
    @Override
    public String toString() {
        return this.codigo_moeda;
    }
    
}
