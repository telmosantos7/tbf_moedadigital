/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author lucas
 */
public class GestaoCreditoController implements Initializable {

    CreditoDao cdtDao = new CreditoDao();

    @FXML
    private TableView<Credito> tableViewGestaoCredito;

    @FXML
    private TableColumn<Credito, Integer> tableColumnId;

    @FXML
    private TableColumn<Credito, String> tableColumnDescricao;

    @FXML
    private TableColumn<Credito, String> tableColumnNome;

    @FXML
    private Button btNovo;

    @FXML
    private Button btEditar;

    @FXML
    private Button btEliminar;

    @FXML
    private Button btRecarregar;

    @FXML
    private ObservableList<Credito> obsList;

    public void onBtNovoAction() {
        System.out.println("not working yet");
    }

    public void onBtEditarAction() {
        System.out.println("not working yet");
    }

    public void onBtEliminarAction() {
        System.out.println("not working yet");
    }

    public void onBtRecarregarAction() {
        System.out.println("not working yet");
    }

    public void updateTableView() {
        List<Credito> list = cdtDao.getAllList();
        obsList = FXCollections.observableArrayList(list);
        tableViewGestaoCredito.setItems(obsList);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initializeNodes();
        // updateTableView();
    }

    public void initializeNodes() {
        tableColumnId.setCellValueFactory(new PropertyValueFactory<>("id_credito"));
        tableColumnDescricao.setCellValueFactory(new PropertyValueFactory<>("descricao_credito"));
        tableColumnNome.setCellValueFactory(new PropertyValueFactory<>("nome_credito"));
        List<Credito> list = cdtDao.getAllList();
        obsList = FXCollections.observableArrayList(list);
        tableViewGestaoCredito.setItems(obsList);

    }
}
