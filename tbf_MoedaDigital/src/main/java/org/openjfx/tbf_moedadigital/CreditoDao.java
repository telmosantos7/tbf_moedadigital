/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lucas
 */
//Criar metodos -> Create, Update, Delete, Select
public class CreditoDao {

    private Connection conn;

    public void insertCredito(Credito obj) {
        PreparedStatement st = null;
        try {
            conn = ConnDB.getConnection();
            st = conn.prepareStatement("INSERT INTO LAPR2_G5.Creditos "
                    + "(id_credito,descricao_credito,nome_credito) "
                    + "VALUES "
                    + "?,?,?");
            st.setInt(1, obj.getId_credito());
            st.setString(2, obj.getDescricao_credito());
            st.setString(3, obj.getNome_credito());

            ResultSet rs = st.executeQuery();
            st.close();
            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }

    }

    public void updateCredito(Credito obj) {
        PreparedStatement st = null;
        try {
            conn = ConnDB.getConnection();
            st = conn.prepareStatement("UPDATE LAPR2_G5.Creditos "
                    + "SET id_credito = ?, SET descricao_credito = ?, SET nome_credito = ? "
                    + "WHERE id_credito = ?");
            st.setInt(1, obj.getId_credito());
            st.setString(2, obj.getDescricao_credito());
            st.setString(3, obj.getNome_credito());
            st.setInt(4, obj.getId_credito());

            ResultSet rs = st.executeQuery();
            st.close();
            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
    }

    public void deleteCredito(Integer id) {
        PreparedStatement st = null;
        try {
            conn = ConnDB.getConnection();
            st = conn.prepareStatement("DELETE FROM LAPR2_G5.Creditos WHERE id_credito = ?");
            st.setInt(1, id);
            st.executeUpdate();;
            ResultSet rs = st.executeQuery();
            st.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ocorreu um erro: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
    }

    public List<Credito> getAllList() {
        List<Credito> list = new ArrayList<>();
        String sql = "SELECT * FROM LAPR2_G5.Creditos";
        try {
            conn = ConnDB.getConnDB();
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Credito credito = new Credito();
                credito.setId_credito(rs.getInt("id_credito"));
                credito.setDescricao_credito(rs.getString("descricao_credito"));
                credito.setNome_credito(rs.getString("nome_credito"));
                list.add(credito);
            }
            st.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println("Ocorreu um erro:" + e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
        return list;
    }

    private Credito instantiateCredito(ResultSet rs) throws SQLException {
        Credito obj = new Credito();
        obj.setId_credito(rs.getInt("id_credito"));
        obj.setNome_credito(rs.getString("nome_credito"));
        obj.setDescricao_credito(rs.getString("descricao_credito"));
        return obj;
    }

}
