/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openjfx.tbf_moedadigital;

import java.io.IOException;
import java.net.URL;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import static org.openjfx.tbf_moedadigital.NovoClienteController.generateRandomCodigoAcesso;
import static org.openjfx.tbf_moedadigital.NovoClienteController.generateRandomUtilizadorNumContrato;
/**
 * FXML Controller class
 *
 * @author Pc
 */
public class RegistarController implements Initializable {


    @FXML
    private TextField txtNumeroContrato;
    @FXML
    private TextField txtPassword;
    @FXML
    private TextField txtNome;
    @FXML
    private TextField txtMorada1;
    @FXML
    private TextField txtNIF;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtIban;
    @FXML
    private Button btnRegistar;
    @FXML
    private Button btnSair;

     private int id_utilizador;
    private String nome_utilizador;
    private String nif_utilizador;
    private String morada_utilizador;
    private String email_utilizador;
    private int utilizador_id_moeda;
    private int tipo_utilizador;
    private String codigo_acesso_utilizador;
    private String utilizador_num_contrato;
    private String iban_utilizador;
    @FXML
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bindForm();
        
    }

    @FXML
    private void btnRegistarOnAction(ActionEvent event) throws IOException, ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
         boolean blank = txtNumeroContrato.getText().isBlank() || txtPassword.getText().isBlank() || txtNome.getText().isBlank() || txtMorada1.getText().isBlank() || txtNIF.getText().isBlank() || txtEmail.getText().isBlank() || txtIban.getText().isBlank();
        if(!blank){
            login();
        }else{
            Helper.showErrorAlert("Campos obrigatórios por preencher!");
        }   
    }
    @FXML
    private void btnSairOnAction(ActionEvent event) throws IOException, ClassNotFoundException, InstantiationException, SQLException, IllegalAccessException {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("TBF Client Management");
        alert.setHeaderText("AVISO: Registe-se antes de Sair!");
        alert.setContentText("Deseja mesmo sair?");
        
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");
        alert.getButtonTypes().setAll( buttonTypeYES ,buttonTypeNO); 
        
        Optional<ButtonType> result = alert.showAndWait();        
        if (result.get() == buttonTypeYES) {
            System.exit(0);
        } else {
            event.consume();

        }
    }


    private void bindForm() {
        txtNumeroContrato.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtNumeroContrato.getText().length() > 32) {
                txtNumeroContrato.deleteNextChar();
                Helper.showInfoAlert("O campo Nome de Utilizador não pode exceder 32 caracteres.");
            }
        });

        txtPassword.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtPassword.getText().length() > 512) {
                txtPassword.deleteNextChar();
                Helper.showInfoAlert("O campo Palavra-Passe não pode exceder os 512 caracteres.");
            }
        });

        txtNome.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtNome.getText().length() > 255) {
                txtNome.deleteNextChar();
                Helper.showInfoAlert("O campo nome não pode exceder os 255 caracteres.");
            }
            
            
        });

        txtMorada1.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtMorada1.getText().length() > 255) {
                txtMorada1.deleteNextChar();
                Helper.showInfoAlert("O campo Morada 1 não pode exceder os 255 caracteres.");
            }
        });

        txtNIF.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtNIF.getText().length() > 9) {
                txtNIF.deleteNextChar();
                Helper.showInfoAlert("O campo Nif não pode exceder os 9 caracteres.");
            }
           
        });

        txtEmail.textProperty().addListener((value, oldValue, newValue) -> {
            if (txtEmail.getText().length() > 255) {
                txtEmail.deleteNextChar();
                Helper.showInfoAlert("O campo Email não pode exceder os 255 caracteres.");
            }
        });
        txtIban.textProperty().addListener((value,oldValue, newValue) ->{
            if(txtIban.getText().length() > 30) {
                txtIban.deleteNextChar();
                Helper.showInfoAlert("O Campo IBAN não pode exceder os 30 caracteres.");
            }
        });
    }

    public void login() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {

        if (validarRegisto()) {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Login.fxml"));
            Parent root = loader.load();

            Scene scn = new Scene(root);
            //  scn.getStylesheets().add("/styles/Styles.css");
            Stage stg = (Stage) btnRegistar.getScene().getWindow();
            stg.setTitle("TBF Client Management");
            Image anotherIcon = new Image("file:TBF.png");
            stg.getIcons().add(anotherIcon);
            stg.setScene(scn);
            stg.show();
        }
    }

    public boolean validarRegisto() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
       
        Connection conn = ConnDB.getConnDB();
        
        id_utilizador = getIdUtilizador() + 1;  //set id next ao ultimo id da bd
        nome_utilizador = txtNome.getText();
        nif_utilizador = txtNIF.getText();
        morada_utilizador = txtMorada1.getText();
        email_utilizador = txtEmail.getText();
        codigo_acesso_utilizador = txtPassword.getText();
        utilizador_num_contrato = txtNumeroContrato.getText();
        iban_utilizador = txtIban.getText();
        utilizador_id_moeda = 1;
        tipo_utilizador = 2;
        
        String cmd = "INSERT INTO LAPR2_G5.Utilizador\n" +
            "(id_utilizador, utilizador_num_contrato, codigo_acesso_utilizador, email_utilizador, nome_utilizador, nif_utilizador, \n" +
            "morada_utilizador, utilizador_id_moeda, saldo_utilizador, tipo_utilizador,iban_utilizador, registo_utilizador)\n" +
            "VALUES\n" +
            "(" +id_utilizador + ",'"+utilizador_num_contrato+"','"+codigo_acesso_utilizador+"','"+ email_utilizador + "','" 
                +nome_utilizador+ "','"+ nif_utilizador+"','"+morada_utilizador+"','"+utilizador_id_moeda+"',0,'"+tipo_utilizador+"','"+iban_utilizador+"',CURRENT_TIMESTAMP);";
        
        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        preparedStatement.executeUpdate(cmd);
        preparedStatement.close();
        Helper.showAlertGravar("Cliente/Admin Criado com sucesso");
    return true;
    }

     public int getIdUtilizador() throws SQLException, ClassNotFoundException{
        
        Connection conn = ConnDB.getConnDB();
        String cmd;
        cmd = "SELECT MAX(id_utilizador) AS id_utilizador FROM LAPR2_G5.Utilizador";
        TipoUtilizador item = new TipoUtilizador();
        
        PreparedStatement preparedStatement = conn.prepareStatement(cmd);
        ResultSet rs = preparedStatement.executeQuery(cmd);
         while (rs.next()) {
                    
                    
                    item.setTipo_utilizador(rs.getInt("id_utilizador"));               
                }
                preparedStatement.close();
                
        id_utilizador = item.getTipo_utilizador();
        System.out.println("id user="+id_utilizador);

    return id_utilizador;
    }

}
        
