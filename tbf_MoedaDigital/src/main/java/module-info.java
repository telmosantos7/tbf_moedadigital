module org.openjfx.tbf_moedadigital {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.base;
    requires java.mail;

       
    opens org.openjfx.tbf_moedadigital to javafx.fxml;
    exports org.openjfx.tbf_moedadigital;  


    
}